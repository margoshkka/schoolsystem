﻿using Microsoft.Practices.Unity;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Modules.Header.Constants;
using SchoolSystem.Desktop.Modules.Header.ViewModels;
using SchoolSystem.Desktop.Modules.Header.Views;

namespace SchoolSystem.Desktop.Modules.Header
{
    public class HeaderModule:Module
    {
        private readonly IEventAggregator eventAggregator;

        public HeaderModule(IUnityContainer container, IRegionManager regionManager, IEventAggregator eventAggregator)
            : base(container, regionManager)
        {
            this.eventAggregator = eventAggregator;
        }

        public override void Initialize()
        {
            base.Initialize();

            RegionManager.RegisterViewWithRegion(RegionNames.HeaderRegion, typeof (MainHeaderView));
            RegisterViews();
            //eventAggregator.GetEvent<AuthorizationStateChangedEvent>().Subscribe(OnAuthorizationStateChanged,
            //    ThreadOption.UIThread, true);
        }

        private void RegisterViews()
        {
            Container.RegisterType<object, MainHeaderView>(ViewNames.MainHeader);
            Container.RegisterType<MainHeaderViewModel>();
        }

        //private void OnAuthorizationStateChanged(AuthorizationStateEventArgs e)
        //{
        //    if (e.IsAuthorized)
        //    {
        //        RegionManager.RequestNavigate(RegionNames.HeaderRegion, ViewNames.MainHeaderView);
        //    }
        //    else
        //    {
        //        RegionManager.Regions[RegionNames.HeaderRegion].Clear();
        //    }
        //}
    }
    
}