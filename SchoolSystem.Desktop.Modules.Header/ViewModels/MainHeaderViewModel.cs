﻿using System;
using System.Net;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Infrastructure.Extensions;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Modules.Content;

namespace SchoolSystem.Desktop.Modules.Header.ViewModels
{
    public class MainHeaderViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private string selectedView;
        public CurrentUser CurrentUser { get; set; }

    

        protected MainHeaderViewModel()
        {}


        public string SelectedView
        {
            get { return selectedView; }
            set
            {
                selectedView = value;
                eventAggregator.GetEvent<HeaderChangedEvent>().Publish(new HeaderChangedEventArgs(value));
            }
        }

        public    MainHeaderViewModel(IEventAggregator eventAggregator,IRegionManager regionManager, CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;

            InitializeCommands();
        }

       public ICommand GetMarksCommand { get; private set; }
       public ICommand GetPupilsCommand { get; private set; }
        public ICommand GetClassCommand { get; private set; }
      //  public ICommand GetTopPupilsCommand { get; private set; }
        public ICommand GetAlumnusCommand { get; private set; }
        public ICommand AuthorizationCommand { get; private set; }
        public ICommand RegistrationCommand { get; private set; }
        public ICommand GetProceduresCommand { get; private set; }
        public ICommand MarkTypesCommand { get; private set; }
        public ICommand LessonStatusesCommand { get; private set; }
        public ICommand ClassTypesCommand { get; private set; }
        //public ICommand GetTopPupilsCommand { get; private set; }
        void InitializeCommands()
        {
           
            GetPupilsCommand=new DelegateCommand(() => SelectedView=HeaderItemNames.PupilsButton);
            GetMarksCommand=new DelegateCommand(() => SelectedView=HeaderItemNames.MarksButton);
            GetClassCommand = new DelegateCommand(() => SelectedView = HeaderItemNames.ClassesButton);
         //   GetTopPupilsCommand = new DelegateCommand(() => SelectedView = HeaderItemNames.TopPupilsButton);
            GetAlumnusCommand=new DelegateCommand(() => SelectedView=HeaderItemNames.AlumnusButton);
            AuthorizationCommand = new DelegateCommand(Authorization);
            RegistrationCommand=new DelegateCommand(() =>SelectedView=HeaderItemNames.RegistrationButton);
            MarkTypesCommand=new DelegateCommand(() => SelectedView=HeaderItemNames.MarkTypesButton);
            ClassTypesCommand = new DelegateCommand(() => SelectedView = HeaderItemNames.ClassTypesButton);
            LessonStatusesCommand = new DelegateCommand(() => SelectedView = HeaderItemNames.LessonStatusesButton);
            GetProceduresCommand=new DelegateCommand(() => SelectedView=HeaderItemNames.ProceduresButton);
        }

        public  void Authorization()
        {
            if (CurrentUser.IsLoggedIn)
            {
                CurrentUser.IsLoggedIn = false;
                CurrentUser.Role = String.Empty;
                regionManager.Regions[RegionNames.HubRegion].Clear();
                regionManager.RequestNavigate(RegionNames.ContentRegion, Content.Constants.ViewNames.LoginView);
                return;
            }
            else
            {
                SelectedView = HeaderItemNames.AuthorizationButton;
            }

        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            
        }
    }
}