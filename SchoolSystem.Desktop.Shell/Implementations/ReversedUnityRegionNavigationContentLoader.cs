﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Prism.Regions;
using Prism.Unity.Regions;

namespace SchoolSystem.Desktop.Shell.Implementations
{
    public class ReversedUnityRegionNavigationContentLoader : UnityRegionNavigationContentLoader
    {
        public ReversedUnityRegionNavigationContentLoader(IServiceLocator serviceLocator, IUnityContainer container) : base(serviceLocator, container)
        {
        }

        protected override IEnumerable<object> GetCandidatesFromRegion(IRegion region, string candidateNavigationContract)
        {
            return base.GetCandidatesFromRegion(region, candidateNavigationContract).Reverse();
        }
    }
}