﻿using System;
using SchoolSystem.Desktop.Infrastructure.Interfaces;
using log4net;
namespace SchoolSystem.Desktop.Shell.Implementations
{
    public class Log4NetLogger : ILogger
    {
        protected ILog Logger
        {
            get; private set;
        }

        public Log4NetLogger(ILog logger)
        {
            Logger = logger;
        }

        public bool IsDebugEnabled => Logger.IsDebugEnabled;
        public bool IsInfoEnabled => Logger.IsInfoEnabled;
        public bool IsErrorEnabled => Logger.IsErrorEnabled;
        public bool IsFatalEnabled => Logger.IsFatalEnabled;
        public bool IsWarnEnabled => Logger.IsWarnEnabled;

        public void Debug(object message)
        {
            if (IsDebugEnabled)
            {
                Logger.Debug(message);
            }
        }

        public void Debug(object message, Exception exception)
        {
            if (IsDebugEnabled)
            {
                Logger.Debug(message, exception);
            }
        }

        public void Info(object message)
        {
            if (IsInfoEnabled)
            {
                Logger.Info(message);
            }
        }

        public void Info(object message, Exception exception)
        {
            if (IsInfoEnabled)
            {
                Logger.Info(message, exception);
            }
        }

        public void Error(object message)
        {
            if (IsErrorEnabled)
            {
                Logger.Error(message);
            }
        }

        public void Error(object message, Exception exception)
        {
            if (IsErrorEnabled)
            {
                Logger.Error(message, exception);
            }
        }

        public void Fatal(object message)
        {
            if (IsFatalEnabled)
            {
                Logger.Fatal(message);
            }
        }

        public void Fatal(object message, Exception exception)
        {
            if (IsFatalEnabled)
            {
                Logger.Fatal(message, exception);
            }
        }

        public void Warn(object message)
        {
            if (IsWarnEnabled)
            {
                Logger.Warn(message);
            }
        }

        public void Warn(object message, Exception exception)
        {
            if (IsWarnEnabled)
            {
                Logger.Warn(message, exception);
            }
        }
    }
}