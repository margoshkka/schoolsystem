﻿using System;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;
using log4net;
using SchoolSystem.Desktop.Infrastructure.Interfaces;

namespace SchoolSystem.Desktop.Shell.Implementations
{
    public class Log4NetFactory:ILogFactory
    {
        public ILogger GetLogger(Type context)
        {
            return new Log4NetLogger(LogManager.GetLogger(context));
        }

        public ILogger GetLogger<TContext>()
        {
            return new Log4NetLogger(LogManager.GetLogger(typeof(TContext)));
        }
    }
}