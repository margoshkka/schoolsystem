﻿using System;
using Prism.Logging;
using SchoolSystem.Desktop.Infrastructure.Interfaces;

namespace SchoolSystem.Desktop.Shell.Implementations
{
    public class Log4NetLoggerFacade : ILoggerFacade
    {
        protected ILogger Logger
        {
            get; private set;
        }

        public Log4NetLoggerFacade(ILogFactory logFactory)
        {
            Logger = logFactory.GetLogger(GetType());
        }

        public void Log(string message, Category category, Priority priority)
        {
            switch (category)
            {
                case Category.Debug:
                    {
                        Logger.Debug(message);
                        break;
                    }
                case Category.Exception:
                    {
                        Logger.Error(message);
                        break;
                    }
                case Category.Info:
                    {
                        Logger.Info(message);
                        break;
                    }
                case Category.Warn:
                    {
                        Logger.Warn(message);
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException("category", category, null);
            }
        }
    }
}