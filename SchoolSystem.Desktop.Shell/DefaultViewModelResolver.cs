﻿using System;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SchoolSystem.Desktop.Shell
{
    public class DefaultViewModelResolver
    {
        /// <exception cref="FormatException">Exception during parsing of a view path.</exception>
        public static Type ResolveView(Type viewType)
        {
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;

            string viewModelPath;
            try
            {
                viewModelPath = ResolveViewModelPath(viewType.FullName, viewType.Name);
            }
            catch (Exception e)
            {
                throw new FormatException("Exception during parsing of a view path.", e);
            }

            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewModelPath, viewAssemblyName);

            return Type.GetType(viewModelName);
        }

        private static string ResolveViewModelPath(string viewPath, string viewName)
        {
            var viewFolder = Regex.Match(viewPath, ".+\\.Views.", RegexOptions.Compiled).Value;
            var viewModelFolder = viewFolder.Substring(0, viewFolder.Length - 6) + "ViewModels.";

            var viewRelativePath = viewPath.Substring(viewFolder.Length, viewPath.Length - viewFolder.Length);
            var viewRealtiveFolderPath = viewRelativePath.Substring(0, viewRelativePath.Length - viewName.Length);

            return viewModelFolder + viewRealtiveFolderPath + viewName + "Model";
        }
    }
}