﻿using System;
using System.Configuration;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using Microsoft.Practices.Unity;
using Moq;
using Prism.Logging;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Unity;
using SchoolSystem.Desktop.Infrastructure.Implementations;
using SchoolSystem.Desktop.Infrastructure.Interfaces;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Modules.Content;
using SchoolSystem.Desktop.Modules.Header;
using SchoolSystem.Desktop.Modules.HubPanel;
using SchoolSystem.Desktop.Services;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Implementations;
using SchoolSystem.Desktop.Shell.Implementations;
using SchoolSystem.Desktop.Shell.ViewModels;
using ContainerControlledLifetimeManager = Microsoft.Practices.Unity.ContainerControlledLifetimeManager;
using InjectionFactory = Microsoft.Practices.Unity.InjectionFactory;
namespace SchoolSystem.Desktop.Shell
{
    public class Bootstrapper:UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            var shell = Container.Resolve<Views.Shell>();

            shell.DataContext = Container.Resolve<ShellViewModel>();

            return shell;
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver(DefaultViewModelResolver.ResolveView);
            ViewModelLocationProvider.SetDefaultViewModelFactory(item => Container.Resolve(item));

            Application.Current.MainWindow = (Window)this.Shell;
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();

            var moduleTypes = new[]
                              {
                                  typeof (ContentModule),
                                  typeof (HeaderModule),
                                  typeof(HubModule)
                              };

            foreach (var type in moduleTypes)
            {
                ModuleCatalog.AddModule(new ModuleInfo(type.Name, type.AssemblyQualifiedName));
            }
        }

        protected override ILoggerFacade CreateLogger()
        {
            return new Log4NetLoggerFacade(new Log4NetFactory());
        }

        protected override void ConfigureContainer()
        {
            RegisterTypeIfMissing(typeof(IRegionNavigationJournal), typeof(NavigationJournal), false);
            RegisterTypeIfMissing(typeof(IRegionNavigationContentLoader), typeof(ReversedUnityRegionNavigationContentLoader), true);

            base.ConfigureContainer();

            ConfigureLogger();
            Mapper.AddProfile<MapperProfile>();

            Container.RegisterType<ShellViewModel>(new ContainerControlledLifetimeManager());
            Container.RegisterType<Views.Shell>(new ContainerControlledLifetimeManager());
            Container.RegisterType<CurrentUser>(new ContainerControlledLifetimeManager());
            //var permissionServiceMock = new Mock<IPermissionsService>();
            //permissionServiceMock.Setup(item => item.GetAccessTokenAsync())
            //                     .Returns(
            //                         Task.FromResult(new AccessToken("dG_5Pks_V2io5T86WzVCyVGaeJgce7x_OCYFrprklzNgeAEgoWQ4u0xsl4YdE7NwKYG8XI8NY0alA8mbxOSBqUG37Yvvaa67HbERY-Iw6CjEFb0quQzxhtps7e2XFqNJXOPgUeJRvufbpUcUkv0IBS9hiuExq2BdTyIM2NFBcrGco8KJ6Em1h5A_oB3zo8Qm9xDwpdWmGNADqrDRw9fHOpxLAN4f0IYioU6UzO-wkxYoHiM1U71yV6Ii-msO3KNwOigpnFS546LYecmIIPlPuwtUP2y_Ukc0EStiQu9m9hI9EDUNgNhBmK-rHocRzXO8sH3w-lyAEFWekUaPPSJvqRRh8NjNqtek9P-M3cJW3bCHBNd48EcFG_jV62TmuomQGKZHQwVfK3RX-j8jkCbHzufMLyJpeqH7VzpOlS2ZixsbLCLI0Y7fnBrSNEl0IjQyHz4adptW3dLVpUKPy7rEzOEbM_s1yVREkTxaHe8heVCX0WlpumKbkqGUmt6FN8fUyfJ6h4YSSkNlTEXMi2yg-w",
            //                         string.Empty, "bearer", "qwerty@example.com", DateTime.Parse("Sat, 23 Jan 2016 11:50:48 GMT"))));
            //Container.RegisterInstance<IPermissionsService>(permissionServiceMock.Object);
            Container.RegisterType<IPermissionsService, PermissionsService>(new ContainerControlledLifetimeManager());

            //Container.RegisterType<IBusyIndicator, DefaultBusyIndicator>(new ContainerControlledLifetimeManager());
            RegisterServices();
            RegisterHttpServices();

        }

        private void RegisterServices()
        {
            foreach (var type in Assembly.GetAssembly(typeof(ServiceAttribute)).GetTypes())
            {
                foreach (var serviceAttribute in type.GetCustomAttributes<ServiceAttribute>())
                {
                    Container.RegisterType(serviceAttribute.InterfaceType, type);
                }
            }
        }

        private void RegisterHttpServices()
        {
            foreach (var type in Assembly.GetAssembly(typeof(HttpServiceAttribute)).GetTypes())
            {
                foreach (var serviceAttribute in type.GetCustomAttributes<HttpServiceAttribute>())
                {
                    Container.RegisterType(serviceAttribute.InterfaceType, type,
                                           new InjectionConstructor("http://localhost:60229",
                                                                    "",
                                                                    Container.Resolve<IPermissionsService>()));
                }
            }
        }

        private void ConfigureLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
            Container.RegisterType<ILogFactory, Log4NetFactory>();

            Application.Current.DispatcherUnhandledException += (s, e) =>
            {
                Container.Resolve<ILogFactory>()
                         .GetLogger(GetType())
                         .Fatal("DispatcherUnhandledException.", e.Exception);
                e.Handled = true;
            };
        }
    }
}