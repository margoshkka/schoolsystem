﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IClassTypesService))]
    public class ClassTypesService:IClassTypesService
    {
        private readonly SchoolDBContext context;

        public ClassTypesService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task AddClassTypeAsync(ClassType newClassType)
        {
            if (newClassType == null)
                throw new ArgumentNullException(nameof(newClassType));
            context.ClassTypes.Add(newClassType);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<List<ClassType>> GetClassTypesAsync()
        {
            return await context.ClassTypes.ToListAsync().ConfigureAwait(false);
        }

        public async Task DeleteClassTypeAsync(int classTypeId)
        {
            var classType = await context.ClassTypes.FindAsync(classTypeId).ConfigureAwait(false);
            if (classType==null)
            {
                throw new NotFoundException($"ClassType with id{classTypeId} wasn`t found");
            }
            context.ClassTypes.Remove(classType);

            await context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}