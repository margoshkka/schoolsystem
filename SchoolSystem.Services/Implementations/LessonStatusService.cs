﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(ILessonStatusService))]
    public class LessonStatusService:ILessonStatusService
    {
        private readonly SchoolDBContext context;

        public LessonStatusService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task<LessonStatus> AddLessonStatusAsync(LessonStatus lessonStatus)
        {
            if (lessonStatus == null)
                throw new ArgumentNullException(nameof(lessonStatus));

            context.LessonStatuses.Add(lessonStatus);
            await context.SaveChangesAsync().ConfigureAwait(false);
            context.Entry(lessonStatus).State = EntityState.Detached;
            return await context.LessonStatuses.FindAsync(lessonStatus.IdStatus).ConfigureAwait(false);
        }

        public async Task DeleteLessonStatusAsync(int idLessonStatus)
        {
            var lessonStetus = await context.LessonStatuses.FindAsync(idLessonStatus).ConfigureAwait(false);
            if (lessonStetus == null)
            {
                throw new NotFoundException($"Lesson status with  id {idLessonStatus} wasn`t found.");
            }
            else
            {
                context.LessonStatuses.Remove(lessonStetus);
                await context.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task<List<LessonStatus>> GetLessonStatuses()
        {

            return await context.LessonStatuses.ToListAsync().ConfigureAwait(false); ;
        }
    }
}