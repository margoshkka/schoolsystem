﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IAlumnusService))]
    public class AlumnusService:IAlumnusService
    {
        private  readonly SchoolDBContext context;

        public AlumnusService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task AddAlumnusAsync(Alumnus alumnus)
        {
            if (alumnus == null)
                throw new ArgumentNullException(nameof(alumnus));
            context.Alumnuses.Add(alumnus);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<List<Alumnus>> GetAlumnusesAsync()
        {
            return await context.Alumnuses.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Alumnus> GetAlumnusAsync(int idAlumnus)
        {
            var alumnus = await context.Alumnuses.FindAsync(idAlumnus).ConfigureAwait(false);
            if (alumnus == null)
            {
                throw new NotFoundException($"Alumnus with alumnus id {idAlumnus} wasn`t found.");
            }

            return alumnus;
        }

        public async Task<List<Alumnus>> GetAlumnusesWithGrantAsync()
        {
            var alumnusesId =await context.GetAlumnusesWithGrants.Select(grant => grant.IdAlumnus).ToListAsync().ConfigureAwait(false);
            return await context.Alumnuses.Where(alumnus => alumnusesId.Contains(alumnus.IdAlumnus)).ToListAsync();
        }

        public async Task<List<Alumnus>> GetAlumnusesWith9ClassesAsync()
        {
            var alumnusesId = await context.GetAlumnusesWithNineClasses.Select(grant => grant.IdAlumnus).ToListAsync().ConfigureAwait(false);
            return await context.Alumnuses.Where(alumnus => alumnusesId.Contains(alumnus.IdAlumnus)).ToListAsync();
        }

        public async Task<double> GetProcentAlumnusesWithGrantAsync(int? year=null)
        {
            var bCount=(context.Alumnuses.Where(alumnus => alumnus.FormOfEducation == "b" && alumnus.YearOfGraduation == year)).Count();
            var allCount=context.Alumnuses.Count();


            return (double) bCount / allCount * 100;
        }

        public  async Task<Alumnus> UpdateAlumnusAsync(int alumnusId, BasicAlumnusInfo alumnusInfo)
        {
            if (alumnusInfo == null)
                throw new ArgumentNullException(nameof(alumnusInfo));
         
            var alumnus = await context.Alumnuses.FindAsync(alumnusId).ConfigureAwait(false);
            if (alumnus == null)
            {
                throw new NotFoundException($"Alumnus with alumnus id {alumnusId} wasn`t found.");
            }
            else
            {
                UpdateAlumnusInfo(alumnus, alumnusInfo);
                await context.SaveChangesAsync().ConfigureAwait(false);
                return alumnus;
            }
        }

        private void UpdateAlumnusInfo(Alumnus alumnus, BasicAlumnusInfo alumnusInfo)
        {
            if (alumnusInfo.Telephone!=null)
            {
                alumnus.Telephone = alumnusInfo.Telephone;
            }
            if (alumnusInfo.Address!=null)
            {
                alumnus.Address = alumnusInfo.Address;
            }
            if (alumnusInfo.FormOfEducation!=null)
            {
                alumnus.FormOfEducation = alumnusInfo.FormOfEducation;
            }
            if (alumnusInfo.InstitutionOfEntry!=null)
            {
                alumnus.InstitutionOfEntry =alumnusInfo.InstitutionOfEntry;
            }
            if (alumnusInfo.NumberOfClasses!=null)
            {
                alumnus.NumberOfClasses = alumnusInfo.NumberOfClasses;
            }
            if (alumnusInfo.Telephone!=null)
            {
                alumnusInfo.Telephone = alumnusInfo.Telephone;
            }
            if (alumnusInfo.Surname!=null)
            {
                alumnus.Surname = alumnusInfo.Surname;
            }
            if (alumnusInfo.YearOfGraduation!=null)
            {
                alumnus.YearOfGraduation = alumnusInfo.YearOfGraduation;
            }
        }

    }
}