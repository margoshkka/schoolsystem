﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(ILessonsService))]
    public class LessonService:ILessonsService
    {
        private readonly SchoolDBContext context;

        public LessonService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task AddLessonAsync(Lesson lesson)
        {
            if (lesson == null)
                throw new ArgumentNullException(nameof(lesson));
            context.Lessons.Add(lesson);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<List<Lesson>> GetLessonsAsync()
        {
            return await context.Lessons.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Lesson> GetLessonAsync(int lessonId)
        {
            var lesson = await context.Lessons.FindAsync(lessonId).ConfigureAwait(false);
            if (lesson == null)
            {
                throw new NotFoundException($"Lesson with lesson id {lessonId} wasn`t found.");
            }

            return lesson;
        }

        public async Task<Lesson> UpdateLesonAsync(int lessonId, BasicLessonInfo lessonInfo)
        {
            if (lessonInfo == null)
                throw new ArgumentNullException(nameof(lessonInfo));
            var lesson = await context.Lessons.FindAsync(lessonId).ConfigureAwait(false);
            if (lesson == null)
            {
                throw new NotFoundException($"Lesson with lesson id {lessonId} wasn`t found.");
            }
            else
            {
                lesson.LessonStatusId = lessonInfo.LessonStatusId;
                await context.SaveChangesAsync().ConfigureAwait(false);
                return lesson;
            }
        }
     }
    
}