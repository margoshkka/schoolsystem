﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IMarkService))]
    public class MarkService:IMarkService
    {

        private readonly SchoolDBContext context;

        public MarkService(SchoolDBContext context)
        {
            this.context = context; 
        }

        public async Task<Mark> AddMarkAsync(Mark mark)
        {
            if (mark == null)
                throw new ArgumentNullException(nameof(mark));
            context.Marks.Add(mark);

            await context.SaveChangesAsync().ConfigureAwait(false);
            context.Entry(mark).State = EntityState.Detached;
            context.Marks.Local.Remove(mark);
            return await  context.Marks.FindAsync(mark.IdMark).ConfigureAwait(false);
        }

        public Task<List<Mark>> FilterMarksAsync(int? subjectId, int? pupilId, int? classId)
        {
            var Marks=context.GetClassJournal(pupilId, classId, subjectId);
            return context.Marks.Where(mark => Marks.Contains(mark.IdMark)).ToListAsync();

        }

      
        public async Task<List<Mark>> GetMarksAsync()
        {
            return await context.Marks.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Mark> GetMarkAsync(int markId)
        {
            var mark = await context.Marks.FindAsync(markId).ConfigureAwait(false);
            if (mark == null)
            {
                throw new NotFoundException($"Mark with mark id {markId} wasn`t found.");
            }

            return mark;
        }
        public async Task<List<Mark>> GetTopPupilsAsync()
        {
            var statistics = (await context.GetSucсessStatistics.ToListAsync().ConfigureAwait(false)).Select(sucсessStatistics => sucсessStatistics.IdMark);

            return await context.Marks.Where(mark => statistics.Contains(mark.IdMark)).ToListAsync().ConfigureAwait(false);
        }
    }
}