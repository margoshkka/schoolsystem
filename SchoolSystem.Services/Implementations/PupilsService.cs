﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IPupilsService))]
    public class PupilsService:IPupilsService
    {
        private readonly SchoolDBContext context;

        public PupilsService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task<List<Pupil>> GetPupilsAsync()
        {
            return await context.Pupils.Where(pupil => !pupil.IsAlumnus).ToListAsync().ConfigureAwait(false);
        }

        public async Task<List<Parent>> GetPupilParentsAsync(int pupilId)
        {
            var pupil = await context.Pupils.FindAsync(pupilId).ConfigureAwait(false);
            if (pupil == null || pupil.IsAlumnus)
            {
                throw new NotFoundException($"Pupil with pupil id {pupilId} wasn`t found.");
            }
            return new List<Parent>() { pupil.Parent, pupil.Parent1 };
        }


        public async Task<Pupil> GetPupilAsync(int pupilId)
        {
            var pupil = await context.Pupils.FindAsync(pupilId).ConfigureAwait(false);
            if (pupil==null ||pupil.IsAlumnus)
            {
                throw new NotFoundException($"Pupil with pupil id {pupilId} wasn`t found.");
            }

            return pupil;
        }

        public async Task<Pupil> UpdatePupilAsync(int pupilId, BasicPupilInfo pupilInfo)
        {
            if (pupilInfo == null)
                throw new ArgumentNullException(nameof(pupilInfo));
            var pupil = await context.Pupils.FindAsync(pupilId).ConfigureAwait(false);
            if (pupil == null)
            {
                throw new NotFoundException($"Pupil with pupil id {pupilId} wasn`t found.");
            }
            else
            {
                UpdatePupilInfo(pupil, pupilInfo);
                await context.SaveChangesAsync().ConfigureAwait(false);
                return pupil;
            }
        }

        private void UpdatePupilInfo(Pupil pupil, BasicPupilInfo pupilInfo)
        {
            if (pupilInfo.Address != null)
                pupil.Address = pupilInfo.Address;
            if (pupilInfo.ClassId != null)
                pupil.ClassId = pupilInfo.ClassId.Value;
            if (pupilInfo.Surname != null)
                pupil.Surname = pupilInfo.Surname;
            if (pupilInfo.Name != null)
                pupil.Name = pupilInfo.Name;
            if (pupilInfo.Telephone != null)
                pupil.Telephone = pupilInfo.Telephone;
        }

        public async Task DeletePupilAsync(int pupilId)
        {
            var pupil =await context.Pupils.FindAsync(pupilId).ConfigureAwait(false);
            if (pupil==null)
            {
                throw new NotFoundException($"Pupil with pupil id {pupilId} wasn`t found.");
            }
            else
            {
                pupil.IsAlumnus = true;
                //context.Pupils.Remove(pupil);
                await context.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task<Pupil> AddPupilAsync(Pupil pupil)
        {
            if (pupil == null)
                throw new ArgumentNullException(nameof(pupil));
            pupil.IsAlumnus = false;
            context.Pupils.Add(pupil);
            await context.SaveChangesAsync().ConfigureAwait(false);
            context.Entry(pupil).State=EntityState.Detached;
            context.Pupils.Local.Remove(pupil);
            return await context.Pupils.FindAsync(pupil.IdPupil).ConfigureAwait(false);
        }

     
    }
}