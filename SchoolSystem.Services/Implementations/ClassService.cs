﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IClassesService))]
    public class ClassService:IClassesService
    {
        private readonly SchoolDBContext context;

        public ClassService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task<Class> AddClassAsync(Class newClass)
        {
            if (newClass == null)
                throw new ArgumentNullException(nameof(newClass));
            context.Classes.Add(newClass);
            await context.SaveChangesAsync().ConfigureAwait(false);
            context.Entry(newClass).State = EntityState.Detached;
            return await context.Classes.FindAsync(newClass.IdClass).ConfigureAwait(false);
        }

        public async Task<List<Class>> GetClassesAsync()
        {
            return await context.Classes.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Class> GetClassAsync(int idClass)
        {
            var @class = await context.Classes.FindAsync(idClass).ConfigureAwait(false);
            if (@class == null)
            {
                throw new NotFoundException($"Class with class id {idClass} wasn`t found.");
            }

            return @class;
        }

        public async Task DeleteClassAsync(int idClass)
        {
            var @class = await context.Classes.FindAsync(idClass).ConfigureAwait(false);
            if (@class == null)
            {
                throw new NotFoundException($"Class with Class id {idClass} wasn`t found.");
            }
            else
            {
                context.Classes.Remove(@class);
                await context.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}