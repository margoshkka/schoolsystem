﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IParentsService))]
    public class ParentsService:IParentsService
    {
        private readonly SchoolDBContext context;

        public ParentsService(SchoolDBContext context)
        {
            this.context = context;
        }


        public async Task<Parent> GetParentAsync(int parentId)
        {
            var parent = await context.Parents.FindAsync(parentId).ConfigureAwait(false);
            if (parent == null)
            {
                throw new NotFoundException($"Parent with parent id {parentId} wasn`t found.");
            }

            return parent;
        }

        public async Task<Parent> UpdateParentAsync(int parentId, BasicParentInfo parentInfo)
        {
            if (parentInfo == null)
                throw new ArgumentNullException(nameof(parentInfo));
            var parent = await context.Parents.FindAsync(parentId).ConfigureAwait(false);
            if (parent == null)
            {
                throw new NotFoundException($"Parent with parent id {parentId} wasn`t found.");
            }
            else
            {
                UpdateParentInfo(parent, parentInfo);
                await context.SaveChangesAsync().ConfigureAwait(false);
                return parent;
            }
        }

        private void UpdateParentInfo(Parent parent, BasicParentInfo parentInfo)
        {
            if (parentInfo.Job!=null)
            {
                parent.Job = parentInfo.Job;
            }
            if (parentInfo.Telephone!=null)
            {
                parent.Telephone = parentInfo.Telephone;
            }
        }

        public async Task AddParentAsync(Parent parent)
        {
            if (parent == null)
                throw new ArgumentNullException(nameof(parent));
            context.Parents.Add(parent);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}