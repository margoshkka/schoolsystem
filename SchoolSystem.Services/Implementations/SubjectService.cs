﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(ISubjectService))]
    public class SubjectService:ISubjectService
    {
        private readonly SchoolDBContext context;

        public SubjectService(SchoolDBContext context)
        {
            this.context = context;
        }


        public async Task AddSubject(Subject newSubject)
        {
            if (newSubject == null)
                throw new ArgumentNullException(nameof(newSubject));
            context.Subjects.Add(newSubject);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<List<Subject>> GetSubjectsAsync()
        {
            return await context.Subjects.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Subject> GetSubjectAsync(int idSubject)
        {
            var subject = await context.Subjects.FindAsync(idSubject).ConfigureAwait(false);
            if (subject == null)
            {
                throw new NotFoundException($"Subject with subject id {idSubject} wasn`t found.");
            }

            return subject;
        }
    }
}