﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Attributes;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;

namespace SchoolSystem.Services.Implementations
{
    [Service(InterfaceType = typeof(IMarkTypesService))]
    public class MarkTypesService:IMarkTypesService
    {

        private readonly SchoolDBContext context;

        public MarkTypesService(SchoolDBContext context)
        {
            this.context = context;
        }

        public async Task AddMarkTypeAsync(MarkType markType)
        {
            if (markType == null)
                throw new ArgumentNullException(nameof(markType));
            context.MarkTypes.Add(markType);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<List<MarkType>> GetMarkTypesAsync()
        {
            return await context.MarkTypes.ToListAsync().ConfigureAwait(false);
        }

        public async Task DeleteMarkTypeAsync(int markTypeId)
        {
            var marktype = await context.MarkTypes.FindAsync(markTypeId).ConfigureAwait(false);
            if (marktype==null)
            {
                throw new NotFoundException($"Marktype with  id {markTypeId} wasn`t found.");

            }
            context.MarkTypes.Remove(marktype);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}