//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolSystem.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class Alumnus
    {
        public int IdAlumnus { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public Nullable<int> YearOfGraduation { get; set; }
        public string InstitutionOfEntry { get; set; }
        public Nullable<int> NumberOfClasses { get; set; }
        public string FormOfEducation { get; set; }
    }
}
