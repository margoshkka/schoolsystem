﻿using System;

namespace SchoolSystem.Services.Core.Models
{
    public class BasicPupilInfo
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public int? ClassId { get; set; }

        
    }
}