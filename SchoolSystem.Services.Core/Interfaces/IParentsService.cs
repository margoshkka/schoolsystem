﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface IParentsService
    {

        
        Task<Parent> GetParentAsync(int parentId);
        Task<Parent> UpdateParentAsync(int parentId, BasicParentInfo parentInfo);
        Task AddParentAsync(Parent parent);
    }
}