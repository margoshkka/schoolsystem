﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface IMarkTypesService
    {
        Task AddMarkTypeAsync(MarkType markType);
        Task<List<MarkType>> GetMarkTypesAsync();
        Task DeleteMarkTypeAsync(int markTypeId);
    }
}