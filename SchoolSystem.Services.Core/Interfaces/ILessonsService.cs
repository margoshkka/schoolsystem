﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface ILessonsService
    {
        Task AddLessonAsync(Lesson lesson);
        Task<List<Lesson>> GetLessonsAsync();
        Task<Lesson> GetLessonAsync(int lessonId);

        Task<Lesson> UpdateLesonAsync(int lessonId,  BasicLessonInfo lessonInfo);

    }
}