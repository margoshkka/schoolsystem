﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface ISubjectService
    {
        Task AddSubject(Subject newSubject);
        Task<List<Subject>> GetSubjectsAsync();
        Task<Subject> GetSubjectAsync(int idSubject);
    }
}