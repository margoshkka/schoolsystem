﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface IAlumnusService
    {
        Task AddAlumnusAsync(Alumnus alumnus);
        Task<List<Alumnus>> GetAlumnusesAsync();
        Task<Alumnus> GetAlumnusAsync(int idAlumnus);
        Task<List<Alumnus>> GetAlumnusesWithGrantAsync();
        Task<List<Alumnus>> GetAlumnusesWith9ClassesAsync();
        Task<double> GetProcentAlumnusesWithGrantAsync(int? year = null);
        Task<Alumnus> UpdateAlumnusAsync(int alumnusId, BasicAlumnusInfo alumnusInfo);
    }
}