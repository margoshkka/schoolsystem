﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface IMarkService
    {
        Task<Mark> AddMarkAsync(Mark mark);
        Task<List<Mark>> FilterMarksAsync(int? subjectId, int? pupilId, int? classId);
        Task<List<Mark>> GetMarksAsync();
        Task<Mark> GetMarkAsync(int markId);

        Task<List<Mark>> GetTopPupilsAsync();

    }
}