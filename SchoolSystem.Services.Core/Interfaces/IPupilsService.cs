﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Models;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface IPupilsService
    {
        Task<List<Parent>> GetPupilParentsAsync(int pupilId);
        Task<List<Pupil>> GetPupilsAsync();
        Task<Pupil> GetPupilAsync(int pupilId);
        Task<Pupil> UpdatePupilAsync(int pupilId,BasicPupilInfo pupilInfo);
        Task DeletePupilAsync(int pupilId);
        Task<Pupil> AddPupilAsync(Pupil pupil);

    }
}