﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Domain;

namespace SchoolSystem.Services.Core.Interfaces
{
    public interface ILessonStatusService
    {
        Task<LessonStatus> AddLessonStatusAsync(LessonStatus lessonStatus);
        Task DeleteLessonStatusAsync(int idLessonStatus);
        Task<List<LessonStatus>> GetLessonStatuses();
    }
}