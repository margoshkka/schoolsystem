﻿using System;
using System.Runtime.Serialization;

namespace SchoolSystem.Services.Core.Exceptions
{
    public class BussinesLogicException : Exception
    {
        public BussinesLogicException()
        {
        }

        public BussinesLogicException(string message) : base(message)
        {
        }

        public BussinesLogicException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BussinesLogicException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}