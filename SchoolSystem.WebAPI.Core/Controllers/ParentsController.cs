﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/parents")]
    public class ParentsController:BaseApiController
    {
        private readonly IParentsService parentsService;

        public ParentsController(IParentsService parentsService)
        {
            this.parentsService = parentsService;
        }




        [HttpGet]
        [Route("{parentId:int}")]
        public async Task<IHttpActionResult> GetParentAsync(int parentId)
        {
            try
            {
                var parent = await parentsService.GetParentAsync(parentId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ParentDto>(parent)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpPost]
        [Route]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator + "," )]
        public async Task<IHttpActionResult> AddParentAsync(ParentDto parentDto)
        {
            var parent = Mapper.Map<Parent>(parentDto);

            await parentsService.AddParentAsync(parent).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ParentDto>(parent)));
        }
        [HttpPatch]
        [Route("{parrentId:int}")]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator + "," )]
        public async Task<IHttpActionResult> UpdateParentAsync(int parentId, [FromBody] BasicParentInfoDto basicParentInfo)
        {
            try
            {
                var parent = Mapper.Map<BasicParentInfo>(basicParentInfo);
                var newParent = await parentsService.UpdateParentAsync(parentId, parent).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<BasicParentInfoDto>(newParent)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}