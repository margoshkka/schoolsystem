﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/alumnuses")]
    public class AlumnusController:BaseApiController
    {
        private readonly IAlumnusService alumnusService;

        public AlumnusController(IAlumnusService alumnusService)
        {
            this.alumnusService = alumnusService;
        }


        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetAlumnusesAsync()
        {
            var alumnuses = await alumnusService.GetAlumnusesAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<AlumnusDto>>(alumnuses)));
        }

        //public async Task<List<Alumnus>> GetAlumnusesWithGrantAsync()
        //{
        //    var alumnusesId = await context.GetAlumnusesWithGrants.Select(grant => grant.IdAlumnus).ToListAsync().ConfigureAwait(false);
        //    return await context.Alumnuses.Where(alumnus => alumnusesId.Contains(alumnus.IdAlumnus)).ToListAsync();
        //}

        //public async Task<List<Alumnus>> GetAlumnusesWith9ClassesAsync()
        //{
        //    var alumnusesId = await context.GetAlumnusesWithNineClasses.Select(grant => grant.IdAlumnus).ToListAsync().ConfigureAwait(false);
        //    return await context.Alumnuses.Where(alumnus => alumnusesId.Contains(alumnus.IdAlumnus)).ToListAsync();
        //}

        //public async Task<int> GetProcentAlumnusesWithGrantAsync(int? year = null)
        //{
        //    return context.GetProcentAlumnusesWithGrant(year);
        //}

        [HttpGet]
        [Route("withgrant")]
        public async Task<IHttpActionResult> GetAlumnusesWithGrantAsync()
        {
            var alumnuses = await alumnusService.GetAlumnusesWithGrantAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<AlumnusDto>>(alumnuses)));
        }
   [HttpGet]
        [Route("withnineclasses")]
        public async Task<IHttpActionResult> GetAlumnusesWith9ClassesAsync()
        {
            var alumnuses = await alumnusService.GetAlumnusesWith9ClassesAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<AlumnusDto>>(alumnuses)));
        }
        [HttpGet]
        [Route("procentwithgrant/{year:int?}")]
        public async Task<IHttpActionResult> GetProcentAlumnusesWithGrantAsync(int? year)
        {
            var procent = await alumnusService.GetProcentAlumnusesWithGrantAsync(year).ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK,procent));
        }


        [HttpGet]
        [Route("{alumnusId:int}")]
        public async Task<IHttpActionResult> GetAlumnusAsync(int alumnusId)
        {
            try
            {
                var alumnus = await alumnusService.GetAlumnusAsync(alumnusId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<AlumnusDto>(alumnus)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpPost]
        [Route]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator)]
        public async Task<IHttpActionResult> AddAlumnusAsync(AlumnusDto alumnusDto)
        {
            var alumnus = Mapper.Map<Alumnus>(alumnusDto);

            await alumnusService.AddAlumnusAsync(alumnus).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<AlumnusDto>(alumnus)));
        }

     
        [HttpPatch]
        [Route("{alumnusId:int}")]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator)]

        public async Task<IHttpActionResult> UpdatePupilAsync(int alumnusId, [FromBody] BasicAlumnusInfoDto basicAlumnusInfoDto)
        {
            try
            {
                var basicAlumnusInfo = Mapper.Map<BasicAlumnusInfo>(basicAlumnusInfoDto);
                var newAlumnus = await alumnusService.UpdateAlumnusAsync(alumnusId, basicAlumnusInfo).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<BasicAlumnusInfoDto>(newAlumnus)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}