﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{

    [RoutePrefix("api/pupils")]
    [Authorize]
    public class PupilsController:BaseApiController
    {
        private readonly IPupilsService pupilsService;
        private readonly IAlumnusService alumnusService;

        public PupilsController(IPupilsService pupilsService, IAlumnusService alumnusService)
        {
            this.pupilsService = pupilsService;
            this.alumnusService = alumnusService;
        }

        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetPupilsAsync()
        {
            var pupils =await pupilsService.GetPupilsAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<PupilDto>>(pupils)));
        }


        [HttpGet]
        [Route("{pupilId:int}/parents")]
        public async Task<IHttpActionResult> GetPupilParentsAsync(int pupilId)
        {
            try
            {
                var pupil = await pupilsService.GetPupilParentsAsync(pupilId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<PupilDto>(pupil)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpGet]
        [Route("{pupilId:int}")]
        public async Task<IHttpActionResult> GetPupilAsync(int pupilId)
        {
            try
            {
                var pupil = await pupilsService.GetPupilAsync(pupilId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<PupilDto>(pupil)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpPost]
        [Route]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator )]
        public async Task<IHttpActionResult> AddPupilAsync(PupilDto pupilDto)
        {
            var pupil = Mapper.Map<Pupil>(pupilDto);

           pupil= await pupilsService.AddPupilAsync(pupil).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<PupilDto>(pupil)));
        }

        [HttpDelete]
        [Route("{pupilId:int}")]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator )]
        public async Task<IHttpActionResult> DeletePupilAsync(int pupilId)
        {
            try
            {
                var pupil = await pupilsService.GetPupilAsync(pupilId).ConfigureAwait(false);
                int classYear = pupil.Class.ClassYear;
                int classNumber = pupil.Class.Number;

                await pupilsService.DeletePupilAsync(pupilId).ConfigureAwait(false);

                await alumnusService.AddAlumnusAsync(new Alumnus()
                                               {
                                                   Telephone = pupil.Telephone, YearOfGraduation =classYear,
                                                   Address = pupil.Address, Name = pupil.Name, SecondName = pupil.SecondName,
                                                   NumberOfClasses = classNumber, Surname = pupil.Surname, DateOfBirth = pupil.DateOfBirth
                                               }).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpPatch]
        [Route("{pupilId:int}")]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator)]

        public async Task<IHttpActionResult> UpdatePupilAsync(int pupilId,[FromBody] BasicPupilInfoDto basicPupilInfo)
        {
            try
            {
                var pupil = Mapper.Map<BasicPupilInfo>(basicPupilInfo);
                var newPupil=await pupilsService.UpdatePupilAsync(pupilId,pupil).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK,Mapper.Map<BasicPupilInfoDto>(newPupil)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}