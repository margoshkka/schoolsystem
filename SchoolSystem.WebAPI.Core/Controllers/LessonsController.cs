﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.Services.Core.Models;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/lessons")]
    public class LessonsController:BaseApiController
    {
        private readonly ILessonsService lessonsService;

        public LessonsController(ILessonsService lessonsService)
        {
            this.lessonsService = lessonsService;
        }


        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetLessonsAsync()
        {
            var lessons = await lessonsService.GetLessonsAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<LessonDto>>(lessons)));
        }


        [HttpGet]
        [Route("{lessonId:int}")]
        public async Task<IHttpActionResult> GetLessonAsync(int lessonId)
        {
            try
            {
                var lesson = await lessonsService.GetLessonAsync(lessonId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<Lesson>(lesson)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator + "," + Roles.Teacher)]
        [HttpPost]
        [Route]
        public async Task<IHttpActionResult> AddLessonAssync(LessonDto lessonDto)
        {
            var lesson = Mapper.Map<Lesson>(lessonDto);

            await lessonsService.AddLessonAsync(lesson).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<LessonDto>(lesson)));
        }


        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator+"," +Roles.Teacher)]
        [HttpPatch]
        [Route("{lessonId:int}")]
        public async Task<IHttpActionResult> UpdateLessonAsync(int lessonId, [FromBody] BasicLessonInfo basicLessonInfo)
        {
            try
            {
                var lesson = Mapper.Map<BasicLessonInfo>(basicLessonInfo);
                var newLesson = await lessonsService.UpdateLesonAsync(lessonId, lesson).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<BasicLessonInfo>(newLesson)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}