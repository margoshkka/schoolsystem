﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/subjects")]
    public class SubjectsController : BaseApiController
    {
        private readonly ISubjectService subjectService;

        public SubjectsController(ISubjectService subjectService)
        {
            this.subjectService = subjectService;
        }


        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetSubjectsAsync()
        {
            var subjects = await subjectService.GetSubjectsAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<SubjectDto>>(subjects)));
        }

        [HttpGet]
        [Route("{subjectId:int}")]
        public async Task<IHttpActionResult> GetSubjectAsync(int subjectId)
        {
            try
            {
                var subject = await subjectService.GetSubjectAsync(subjectId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<SubjectDto>(subject)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpPost]
        [Route]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator )]
        public async Task<IHttpActionResult> AddSubjectAsync(SubjectDto subjectDto)
        {
            var subject = Mapper.Map<Subject>(subjectDto);

            await subjectService.AddSubject(subject).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<SubjectDto>(subject)));
        }
    }
}