﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/marks")]
    public class MarksController:BaseApiController
    {
        private readonly IMarkService markService;

        public MarksController(IMarkService markService)
        {
            this.markService = markService;
        }




        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetMarksAsync()
        {
            var marks = await markService.GetMarksAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<MarkDto>>(marks)));
        }


        [HttpGet]
        [Route("{markId:int}")]
        public async Task<IHttpActionResult> GetMarkAsync(int markId)
        {
            try
            {
                var mark = await markService.GetMarkAsync(markId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<MarkDto>(mark)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

        [HttpPost]
        [Route]
        [Authorize(Roles = Roles.MainTeacher + "," + Roles.Administrator + "," + Roles.Teacher)]
        public async Task<IHttpActionResult> AddMarkAsync(MarkDto markDto)
        {
            var mark = Mapper.Map<Mark>(markDto);

            mark= await markService.AddMarkAsync(mark).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<MarkDto>(mark)));
        }


        [HttpGet]
        [Route("filter")]
        [Authorize]
        public async Task<IHttpActionResult> FilterMarksAsync(int? pupilId=null, int?subjectId=null ,int? classId=null)
        {

            var marks=await markService.FilterMarksAsync(subjectId,pupilId,classId).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<MarkDto>>(marks)));
        }

        [HttpGet]
        [Route("toppupils")]
        public async Task<IHttpActionResult> GetTopPupilsAsync()
        {
            var marks = await markService.GetTopPupilsAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<MarkDto>>(marks)));
        }

    }
}