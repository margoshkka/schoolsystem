﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/classes")]
    public class ClassesController:BaseApiController
    {
        private readonly IClassesService classesService;

        public ClassesController(IClassesService classesService)
        {
            this.classesService = classesService;
        }


        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetClassesAsync()
        {
            var classes = await classesService.GetClassesAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<ClassDto>>(classes)));
        }

        [HttpGet]
        [Route("{classId:int}")]
        public async Task<IHttpActionResult> GetClassAsync(int classId)
        {
            try
            {
                var @class= await classesService.GetClassAsync(classId).ConfigureAwait(false);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ClassDto>(@class)));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,CreateErrorResult(e.Message)));
            }
        }

        [Authorize (Roles=Roles.MainTeacher+","+Roles.Administrator)]
        [HttpPost]
        [Route]
        public async Task<IHttpActionResult> AddClassAsync(ClassDto classDto)
        {
            var @class = Mapper.Map<Class>(classDto);

           @class= await classesService.AddClassAsync(@class).ConfigureAwait(false);
                
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ClassDto>(@class)));
        }

        [Authorize(Roles =  Roles.Administrator)]
        [HttpDelete]
        [Route("{classId:int}")]
        public async Task<IHttpActionResult> DeleteClassAsync(int classId)
        {
            try
            {
                await classesService.DeleteClassAsync(classId).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}