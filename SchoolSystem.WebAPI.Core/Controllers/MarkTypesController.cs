﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [Authorize]
    [RoutePrefix("api/marktypes")]
    public class MarkTypesController:BaseApiController
    {
        private readonly IMarkTypesService markTypesService;

        public MarkTypesController(IMarkTypesService markTypesService)
        {
            this.markTypesService = markTypesService;
        }

        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetMarkTypesAsync()
        {
            var marktypes = await markTypesService.GetMarkTypesAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<MarkTypeDto>>(marktypes)));
        }


       
        [HttpPost]
        [Route]
        [Authorize(Roles =  Roles.Administrator)]
        public async Task<IHttpActionResult> AddMarkTypeAsync(MarkTypeDto markTypeDto)
        {
            var marktype = Mapper.Map<MarkType>(markTypeDto);

            await markTypesService.AddMarkTypeAsync(marktype).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<MarkTypeDto>(marktype)));
        }

        [HttpDelete]
        [Route("{markTypeId:int}")]
        [Authorize(Roles = Roles.Administrator)]
        public async Task<IHttpActionResult> DeletMarkTypeAsync(int markTypeId)
        {
            try
            {

                await markTypesService.DeleteMarkTypeAsync(markTypeId).ConfigureAwait(false);

               
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}