﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    [RoutePrefix("api/lessonstatuses")]
    [Authorize]
    public class LessonStatusController:BaseApiController
    {
        private readonly ILessonStatusService lessonStatusService;

        public LessonStatusController(ILessonStatusService lessonStatusService)
        {
            this.lessonStatusService = lessonStatusService;
        }

        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetLessonStatusesAsync()
        {
            var lessonStatuses = await lessonStatusService.GetLessonStatuses().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<LessonStatusDto>>(lessonStatuses)));
        }


        [HttpPost]
        [Route]
        [Authorize(Roles =  Roles.Administrator)]
        public async Task<IHttpActionResult> AddLessonStatusAsync(LessonStatusDto lessonStatusDto)
        {
            var lessonStatus = Mapper.Map<LessonStatus>(lessonStatusDto);

            lessonStatus = await lessonStatusService.AddLessonStatusAsync(lessonStatus).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<LessonStatusDto>(lessonStatus)));
        }

        [HttpDelete]
        [Route("{lessonStatusId:int}")]
        [Authorize(Roles =  Roles.Administrator)]
        public async Task<IHttpActionResult> DeleteLessonStatusAsync(int lessonStatusId)
        {
            try
            {
               await lessonStatusService.DeleteLessonStatusAsync(lessonStatusId).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }
    }
}