﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Exceptions;
using SchoolSystem.Services.Core.Interfaces;
using SchoolSystem.WebAPI.Core.Constants;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    
    [Authorize]
    [RoutePrefix("api/classtypes")]
    public class ClassTypesController:BaseApiController
    {
        private readonly IClassTypesService classTypesService;

        public ClassTypesController(IClassTypesService classTypesService)
        {
            this.classTypesService = classTypesService;
        }

        [HttpGet]
        [Route]
        public async Task<IHttpActionResult> GetClassTypesAsync()
        {
            var types = await classTypesService.GetClassTypesAsync().ConfigureAwait(false);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<List<ClassTypeDto>>(types)));
        }


        [HttpPost]
        [Route]
        [Authorize(Roles =  Roles.Administrator)]
        public async Task<IHttpActionResult> AddClassTypesAsync(ClassTypeDto classTypeDto)
        {
            var classType = Mapper.Map<ClassType>(classTypeDto);

            await classTypesService.AddClassTypeAsync(classType).ConfigureAwait(false);

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<ClassTypeDto>(classType)));
        }

        [HttpDelete]
        [Route("{classTypeId:int}")]
        [Authorize(Roles =  Roles.Administrator)]
        public async Task<IHttpActionResult> DeleteClassTypeAsync(int classTypeId)
        {
            try
            {
                await classTypesService.DeleteClassTypeAsync(classTypeId).ConfigureAwait(false);

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            catch (NotFoundException e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, CreateErrorResult(e.Message)));
            }
        }

    }
}