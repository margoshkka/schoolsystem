﻿using System.Web.Http;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core.Controllers
{
    public class BaseApiController : ApiController
    {
        protected virtual ErrorDto CreateErrorResult(string message)
        {
            return new ErrorDto(message);
        }
        
    }
}