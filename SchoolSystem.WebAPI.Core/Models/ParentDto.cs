﻿namespace SchoolSystem.WebAPI.Core.Models
{
    public class ParentDto
    {
        public int IdParent { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public string Telephone { get; set; }
        public string Sex { get; set; }
        public string Job { get; set; }

    }
}