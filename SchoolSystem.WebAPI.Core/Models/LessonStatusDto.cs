﻿namespace SchoolSystem.WebAPI.Core.Models
{
    public class LessonStatusDto
    {
        public int IdStatus { get; set; }
        public string StatusName { get; set; }
    }
}