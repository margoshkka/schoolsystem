﻿namespace SchoolSystem.WebAPI.Core.Models
{
    public class MarkTypeDto
    {
        public int IdTypeOfMark { get; set; }
        public string Name { get; set; }
    }
}