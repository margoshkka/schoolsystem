﻿using System;

namespace SchoolSystem.WebAPI.Core.Models
{
    public class AlumnusDto
    {

        
            public int IdAlumnus { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string SecondName { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string Telephone { get; set; }
            public string Address { get; set; }
            public int? YearOfGraduation { get; set; }
            public string InstitutionOfEntry { get; set; }
            public int? NumberOfClasses { get; set; }
            public string FormOfEducation { get; set; }
}
}