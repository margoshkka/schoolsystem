﻿namespace SchoolSystem.WebAPI.Core.Models
{
    public class BasicParentInfoDto
    {
        public string Telephone { get; set; }
        public string Job { get; set; }
    }
}