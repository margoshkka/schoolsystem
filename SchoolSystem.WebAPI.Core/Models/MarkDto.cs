﻿using System;
using SchoolSystem.Domain;

namespace SchoolSystem.WebAPI.Core.Models
{
    public class MarkDto
    {
        public int IdMark { get; set; }
        public int PupilId { get; set; }
        public int SubjectId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> MarkValue { get; set; }
        public int MarkTypeId { get; set; }

        public virtual MarkTypeDto MarkType { get; set; }
        public virtual PupilDto Pupil { get; set; }
        public virtual SubjectDto Subject { get; set; }
     
    }
}