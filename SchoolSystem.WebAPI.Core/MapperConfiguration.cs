﻿using AutoMapper;
using SchoolSystem.Domain;
using SchoolSystem.Services.Core.Models;
using SchoolSystem.WebAPI.Core.Models;

namespace SchoolSystem.WebAPI.Core
{
    public class MapperConfiguration:Profile
    {
        void Register<TSource, TValue>()
        {
            Mapper.CreateMap<TSource, TValue>();
            Mapper.CreateMap<TValue, TSource>();
        }
        protected override void Configure()
        {
            Register<PupilDto,Pupil>();
            Register<BasicPupilInfoDto, BasicPupilInfo>();
            Mapper.CreateMap<Pupil, BasicPupilInfoDto>();
            Mapper.CreateMap<Lesson, BasicLessonInfoDto>();
            Register<Lesson,LessonDto>();
            Register<Parent,ParentDto>();
            Register<BasicParentInfo, BasicParentInfoDto>();
            Mapper.CreateMap<Parent, BasicParentInfoDto>();
            Register<BasicLessonInfo, BasicLessonInfoDto>();
            Register<Subject, SubjectDto>();
            Register<ClassDto, Class>();
            Register<MarkType, MarkTypeDto>();
            Register<Mark, MarkDto>();
            Register<LessonStatusDto, LessonStatus>();
            Register<ClassType, ClassTypeDto>();
            Register<Alumnus, AlumnusDto>();
            Register<BasicAlumnusInfo, BasicAlumnusInfoDto>();
            Mapper.CreateMap<Alumnus, BasicAlumnusInfoDto>();
        }
    }
}