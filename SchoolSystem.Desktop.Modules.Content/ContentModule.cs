﻿using System;
using SchoolSystem.Desktop.Infrastructure.Base;
using Microsoft.Practices.Unity;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Infrastructure.Extensions;
using SchoolSystem.Desktop.Modules.Content.Constants;
using SchoolSystem.Desktop.Modules.Content.ViewModels;
using SchoolSystem.Desktop.Modules.Content.Views;
using NavigationParameters = Prism.Regions.NavigationParameters;


namespace SchoolSystem.Desktop.Modules.Content
{
    public class ContentModule:Module
    {
        private readonly IEventAggregator eventAggregator;

        public ContentModule(IUnityContainer container, IRegionManager regionManager, IEventAggregator eventAggregator)
            : base(container, regionManager)
        {
            this.eventAggregator = eventAggregator;
        }

        public override void Initialize()
        {
            base.Initialize();

            RegionManager.RegisterViewWithRegion(RegionNames.ContentRegion, typeof (LoginContentView));
            RegisterViews();
            eventAggregator.GetEvent<HeaderChangedEvent>().Subscribe(OnHeaderChanged,
                ThreadOption.UIThread, true);
            eventAggregator.GetEvent<HubPanelChangedEvent>().Subscribe(OnHubPanelChanged, ThreadOption.UIThread, true);
        }

        private void OnHubPanelChanged(HubPanelChangedEventArgs obj)
        {
            switch (obj.SectionName)
            {
                case HubItemNames.ClassJournalButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.ClassJournalView, UriKind.Relative),
                                                  new NavigationParameters() {{Constants.NavigationParameters.ClassParametr,obj.DataContext}});
                    break;
                case HubItemNames.ClassPupilsButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.ClassPupilsView, UriKind.Relative),
                                                  new NavigationParameters() {{Constants.NavigationParameters.ClassParametr, obj.DataContext}});
                    break;
                case HubItemNames.PupilMarksButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.PupilMarksView,UriKind.Relative),
                                                  new NavigationParameters() {{Constants.NavigationParameters.PupilParametr, obj.DataContext}});

                    break;
                case HubItemNames.PupilContactInfoButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.PupilContacInfoView,UriKind.Relative),
                                                  new NavigationParameters() { { Constants.NavigationParameters.PupilParametr, obj.DataContext } });
                    break;
                case HubItemNames.PupilMissedLessonsButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.PupilMissedLessonsView,UriKind.Relative),
                                                  new NavigationParameters() { { Constants.NavigationParameters.PupilParametr, obj.DataContext } });
                    break;
                case HubItemNames.PupilPersonalDealButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.ProcedurePupilData);
                    break;
                case HubItemNames.TimesheetButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.ProcedureTimesheet);
                    break;
                case HubItemNames.MarksJournalButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.ProcedureMarksJournal);
                    break;
                case HubItemNames.MarkReportButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.ProcedureMarksReport);
                    break;
                case HubItemNames.RateCompetitionWorks:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.ProcedureRateWorks);
                    break;
                case HubItemNames.InstitutesButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.Institutes);
                    break;
                case HubItemNames.GrantPupilsButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.GrantPupils);
                    break;

                case HubItemNames.AlumnusWith9ClassesButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.AlumnusWith9Classes);
                    break;

            }
        }

        private void OnHeaderChanged(HeaderChangedEventArgs obj)
        {
            switch (obj.SectionName)
            {
                case HeaderItemNames.AuthorizationButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion,ViewNames.LoginView);
                    break;
                case HeaderItemNames.PupilsButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.PupilsView);
                    break;
                case HeaderItemNames.MarksButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.MarksView);
                    break;
                case HeaderItemNames.ClassesButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.ClassesView);
                    break;
                case HeaderItemNames.AlumnusButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.AlumnusesView);
                    break;
                case HeaderItemNames.RegistrationButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.RegistrationView);
                    break;
                case HeaderItemNames.MarkTypesButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.MarkTypesContentView);
                    break;
                case HeaderItemNames.ClassTypesButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.ClassTypesContentView);
                    break;
                case HeaderItemNames.LessonStatusesButton:
                    RegionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.LessonStatusesContentView);
                    break;
                case HeaderItemNames.ProceduresButton:
                    RegionManager.Regions[RegionNames.ContentRegion].Clear();
                    break;

            }
        }

        private void RegisterViews()
        {
            Container.RegisterType<object, LoginContentView>(ViewNames.LoginView);
            Container.RegisterType<LoginContentViewModel>();

            Container.RegisterType<object, PupilMissedLessonsContentView>(ViewNames.PupilMissedLessonsView);
            Container.RegisterType<PupilContactInfoContentViewModel>();

            Container.RegisterType<object, PupilContactInfoContentView>(ViewNames.PupilContacInfoView);
            Container.RegisterType<PupilContactInfoContentViewModel>();

            Container.RegisterType<object, PupilMarksContentView>(ViewNames.PupilMarksView);
            Container.RegisterType<PupilMarksContentViewModel>();

            Container.RegisterType<object, ClassJournalContentView>(ViewNames.ClassJournalView);
            Container.RegisterType<ClassJournalContentViewModel>();
            Container.RegisterType<object, ClassPupilsContentView>(ViewNames.ClassPupilsView);
            Container.RegisterType<ClassPupilsContentViewModel>();

            Container.RegisterType<object, ClassesContentView>(ViewNames.ClassesView);
            Container.RegisterType<ClassJournalContentView>();

            Container.RegisterType<object, MarksContentView>(ViewNames.MarksView);
            Container.RegisterType<MarksContentViewModel>();

            Container.RegisterType<object, AlumnusesContentView>(ViewNames.AlumnusesView);
            Container.RegisterType<AlumnusesContentViewModel>();

            Container.RegisterType<object, RegistrationContentView>(ViewNames.RegistrationView);
            Container.RegisterType<RegistrationContentViewModel>();

            Container.RegisterType<object, MarkTypesContentView>(ViewNames.MarkTypesContentView);
            Container.RegisterType<MarkTypesContentViewModel>();

            Container.RegisterType<object, ClassTypesContentView>(ViewNames.ClassTypesContentView);
            Container.RegisterType<ClassTypesContentViewModel>();

            Container.RegisterType<object, PupilsContentView>(ViewNames.PupilsView);
            Container.RegisterType<PupilsContentViewModel>();

            Container.RegisterType<object, LessonStatusesView>(ViewNames.LessonStatusesContentView);
            Container.RegisterType<LessonStatusesViewModel>();

            Container.RegisterType<object, ProcedurePupilsDataView>(ViewNames.ProcedurePupilData);
            Container.RegisterType<ProcedurePupilsDataViewModel>();

            Container.RegisterType<object, ProcedureTimesheetView>(ViewNames.ProcedureTimesheet);
            Container.RegisterType<ProcedureTimesheetViewModel>();

            Container.RegisterType<object, ProcedureMarksJournalView>(ViewNames.ProcedureMarksJournal);
            Container.RegisterType<ProcedureMarksJournalViewModel>();

            Container.RegisterType<object, ProcedureMarkReportView>(ViewNames.ProcedureMarksReport);
            Container.RegisterType<ProcedureMarkReportViewModel>();

            Container.RegisterType<object, ProcedureRateWorksView>(ViewNames.ProcedureRateWorks);
            Container.RegisterType<ProcedureRateWorksViewModel>();

            Container.RegisterType<object, ProcedureInstitutesView>(ViewNames.Institutes);
            Container.RegisterType<ProcedureInstitutesViewModel>();

            Container.RegisterType<object, ProcedureGrantPupilsView>(ViewNames.GrantPupils);
            Container.RegisterType<ProcedureGrantPupilsViewModel>();

            Container.RegisterType<object, ProcedureAlumnusWith9ClassesView>(ViewNames.AlumnusWith9Classes);
           Container.RegisterType<ProcedureAlumnusWith9ClassesViewModel>();

        }

        //private void OnAuthorizationStateChanged(AuthorizationStateEventArgs e)
        //{
        //    if (e.IsAuthorized)
        //    {
        //        RegionManager.RequestNavigate(RegionNames.HeaderRegion, ViewNames.MainHeaderView);
        //    }
        //    else
        //    {
        //        RegionManager.Regions[RegionNames.HeaderRegion].Clear();
        //    }
        //}
    }
}
