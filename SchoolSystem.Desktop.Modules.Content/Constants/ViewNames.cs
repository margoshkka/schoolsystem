﻿namespace SchoolSystem.Desktop.Modules.Content.Constants
{
    public static class ViewNames
    {
        public const string LoginView = "LoginView";
        public const string PupilContacInfoView = "PupilContacInfoView";
        public const string PupilMissedLessonsView = "PupilMissedLessonsView";
        public const string PupilMarksView = "PupilMarksView";
        public const string PupilsView = "PupilsView";
        public const string ClassPupilsView = "ClassPupilsView";
        public const string ClassJournalView = "ClassJournalView";
        public const string ClassesView = "ClassesView";

        public const string MarksView = "MarksContentView";
        public const string AlumnusesView = "AlumnusesView";
        public const string AlumnusContactInfoView = "AlumnusContactInfoView";

        public const string RegistrationView = "RegistrationView";
        public const string MarkTypesContentView = "MarkTypesContentView";
        public const string ClassTypesContentView = "ClassTypesContentView";
        public const string LessonStatusesContentView = "LessonStatusesContentView";

        public const string ProcedurePupilData = "ProcedurePupilData";
        public const string ProcedureTimesheet = "ProcedureTimesheet";
        public const string ProcedureMarksJournal = "ProcedureMarksJournal";
        public const string ProcedureMarksReport = "ProcedureMarksReport";
        public const string ProcedureRateWorks = "ProcedureRateWorks";
        public const string Institutes = "Institutes";
        public const string GrantPupils = "GrantPupils";
        public const string AlumnusWith9Classes = "AlumnusWith9Classes";


    }
}