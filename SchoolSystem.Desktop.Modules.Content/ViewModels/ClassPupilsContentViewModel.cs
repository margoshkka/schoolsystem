﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Modules.Content.Constants;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using Syncfusion.Windows.Shared;
using NavigationParameters = Prism.Regions.NavigationParameters;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ClassPupilsContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IPupilsService pupilsService;


        public Class Class { get; set; }
        public ObservableCollection<Pupil> Pupils { get; set; }
        public ICommand GetPupilCommand { get; set; }



        #endregion

        #region Constructors   

        public ClassPupilsContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IPupilsService pupilsService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.pupilsService = pupilsService;

            Pupils=new ObservableCollection<Pupil>();

            GetPupilCommand = new DelegateCommand<Pupil>(OpenPupil);
        }

        private void OpenPupil(Pupil obj)
        {
            obj.Class = Class;
            eventAggregator.GetEvent<ContentChangedEvent>().Publish(new ContentChangedEventArgs(ContentItemNames.PupilItem,obj));
            regionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.PupilContacInfoView, UriKind.Relative),
                                          new NavigationParameters() { { Constants.NavigationParameters.PupilParametr, obj } });
        }

        protected ClassPupilsContentViewModel()
        { }
        #endregion

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Class @class = navigationContext.Parameters[Constants.NavigationParameters.ClassParametr] as Class;
            if (@class == null)
                throw new ArgumentNullException(nameof(@class));
            Class = @class;

            Pupils.Clear();
            

            Pupils.AddRange((await pupilsService.GetPupilsAsync()).Where(pupil => pupil.ClassId == Class.IdClass));


        }


    }
}