﻿using System;
using System.Linq;
using System.Windows.Input;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class PupilContactInfoContentViewModel:BaseViewModel
    {

        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IPupilsService pupilsService;
        private Pupil pupil;

        public Pupil Pupil
        {
            get { return pupil; }
            set
            {
                if (Equals(value, pupil))
                    return;
                pupil = value;
                OnPropertyChanged();
            }
        }

        //public ICommand GetPupilCommand { get; set; }

        #endregion

        #region Constructors   

        public PupilContactInfoContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager,IPupilsService pupilsService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.pupilsService = pupilsService;
        }
        protected PupilContactInfoContentViewModel()
        {}
        #endregion

        public override  void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Pupil pupil = navigationContext.Parameters[Constants.NavigationParameters.PupilParametr] as Pupil;
            if (pupil == null)
                throw new ArgumentNullException(nameof(pupil));
            Pupil = pupil;



        }
    }
}