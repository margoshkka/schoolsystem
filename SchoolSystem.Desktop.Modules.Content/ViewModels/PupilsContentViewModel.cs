﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class PupilsContentViewModel : BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IPupilsService pupilsService;
        private readonly IParentsService parentsService;
        private Pupil newPupil;
        private bool isAdding;
        private bool isUpdating;
        private Pupil selectedPupil;
        private BasicPupilInfo editingPupil;
        private Parent newFather;

        #endregion

        #region Properties, Indexers

        public ObservableCollection<Pupil> Pupils { get; set; }

        public ICommand UpdateCommand { get; set; }
        public ICommand AddCommand { get; set; }

        public ICommand UpdatePupilCommand { get; set; }
        public ICommand AddPupilCommand { get; set; }
        public ICommand AddFatherCommand { get; set; }
        public ICommand AddMotherCommand { get; set; }
        public ICommand DeletePupilCommand { get; set; }

        public Pupil NewPupil
        {
            get { return newPupil; }
            set
            {
                if (Equals(value, newPupil))
                    return;
                newPupil = value;
                OnPropertyChanged();
            }
        }

        public Parent NewFather
        {
            get { return newFather; }
            set
            {
                if (Equals(value, newFather))
                    return;
                newFather = value;
                OnPropertyChanged();
            }
        }

        public Parent NewMother { get; set; }

        public Pupil SelectedPupil
        {
            get { return selectedPupil; }
            set
            {
                if (Equals(value, selectedPupil))
                    return;
                selectedPupil = value;
                OnPropertyChanged();
            }
        }

        public CurrentUser CurrentUser { get; set; }

        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (value == isAdding)
                    return;
                isAdding = value;
                OnPropertyChanged();
            }
        }

        public bool IsUpdating
        {
            get { return isUpdating; }
            set
            {
                if (value == isUpdating)
                    return;
                isUpdating = value;
                OnPropertyChanged();
            }
        }

        public BasicPupilInfo EditingPupil
        {
            get { return editingPupil; }
            set
            {
                if (Equals(value, editingPupil))
                    return;
                editingPupil = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        public PupilsContentViewModel(IEventAggregator eventAggregator, IPupilsService pupilsService,
                                      IRegionManager regionManager, CurrentUser currentUser,
                                      IParentsService parentsService)
        {
            CurrentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.pupilsService = pupilsService;
            this.regionManager = regionManager;
            this.parentsService = parentsService;

            Pupils = new ObservableCollection<Pupil>();
            NewFather=new Parent() {Sex = "Man"};
            NewMother=new Parent() {Sex = "Woman"};
            
            InitializeCommands();
        }

        protected PupilsContentViewModel()
        {}

        #endregion

        #region Methods

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            Pupils.Clear();


            Pupils.AddRange(await pupilsService.GetPupilsAsync());
        }

        private void InitializeCommands()
        {
            AddCommand = new DelegateCommand(() =>
                                             {
                                                 NewPupil = new Pupil();
                                                 IsAdding = true;

                                             });
            UpdateCommand = new DelegateCommand(() =>
                                                {
                                                    if (SelectedPupil != null)
                                                    {
                                                        EditingPupil = new BasicPupilInfo()
                                                                       {
                                                                           Address = SelectedPupil.Address,
                                                                           Name = SelectedPupil.Name,
                                                                           Surname = SelectedPupil.Surname,
                                                                           Telephone = SelectedPupil.Telephone,
                                                                           ClassId = SelectedPupil.ClassId
                                                                       };
                                                    
                                                    IsUpdating = true;
                                                }
                                                    
                                                });

            AddPupilCommand = DelegateCommand.FromAsyncHandler(AddPupil);
            DeletePupilCommand = DelegateCommand.FromAsyncHandler(DeletePupil);
            UpdatePupilCommand = DelegateCommand.FromAsyncHandler(UpdatePupil);

            AddFatherCommand=DelegateCommand.FromAsyncHandler(AddFather);
            AddMotherCommand=DelegateCommand.FromAsyncHandler(AddMother);

        }

        private async Task AddMother()
        {
            try
            {

                await parentsService.AddParentAsync(NewMother);
                NewPupil.MotherId = NewMother.IdParent;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private async Task AddFather()
        {

            await parentsService.AddParentAsync(NewFather);
            NewPupil.FatherId = NewFather.IdParent;
            
        }

        private async Task DeletePupil()
        {
            try
            {
                if (SelectedPupil!=null)
                {
                    
                    await pupilsService.DeletePupilAsync(SelectedPupil.IdPupil);
                     Pupils.Remove(SelectedPupil);
                }
                
            }
            catch (Exception e)
            {
             
            }
        }

        private async Task UpdatePupil()
        {
            try
            {

                await pupilsService.UpdatePupilAsync(SelectedPupil.IdPupil, EditingPupil);
                isUpdating = false;
                SelectedPupil.Address = EditingPupil.Address;
                SelectedPupil.Name = EditingPupil.Name;
                SelectedPupil.Surname = EditingPupil.Surname;
                SelectedPupil.Telephone = EditingPupil.Telephone;
                SelectedPupil.ClassId = EditingPupil.ClassId.Value;
            }
            catch (Exception e)
            {
            }
        }

        private async Task AddPupil()
        {

            PupilClear(NewPupil);
            NewPupil = await pupilsService.AddPupilAsync(NewPupil);
            isAdding = false;
            Pupil newPupil = Mapper.Map<Pupil>(NewPupil);
            Pupils.Add(newPupil);
            
        }

        
        void PupilClear(Pupil pupil)
        {
            pupil.Class = null;
            pupil.IdPupil = 0;
            pupil.Parent = null;
            pupil.Parent1 = null;
            //pupil.YearOfStartEducation = null;
        }

        #endregion
    }
}