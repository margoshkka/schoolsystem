﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class MarkTypesContentViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IMarkTypesService markTypesService;
        private MarkType newMarkType;
        private bool isAdding;
        private MarkType selectedMarkType;

        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (value == isAdding)
                    return;
                isAdding = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand AddMarkTypeCommand { get; set; }
        public ICommand DeleteMarkTypeCommand { get; set; }
        public CurrentUser CurrentUser { get; set; }
        public ObservableCollection<MarkType> MarkTypes { get; set; }

        public MarkType NewMarkType
        {
            get { return newMarkType; }
            set
            {
                if (Equals(value, newMarkType))
                    return;
                newMarkType = value;
                OnPropertyChanged();
            }
        }

        public MarkType SelectedMarkType
        {
            get { return selectedMarkType; }
            set
            {
                if (Equals(value, selectedMarkType))
                    return;
                selectedMarkType = value;
                OnPropertyChanged();
            }
        }

        public MarkTypesContentViewModel(IRegionManager regionManager, IEventAggregator eventAggregator, IMarkTypesService markTypesService,
            CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;
            this.markTypesService = markTypesService;
            MarkTypes=new ObservableCollection<MarkType>();

            AddCommand = new DelegateCommand(() =>
            {
                NewMarkType = new MarkType();
                IsAdding = true;

            });
            AddMarkTypeCommand = DelegateCommand.FromAsyncHandler(AddMarkType);
            DeleteMarkTypeCommand = DelegateCommand.FromAsyncHandler(DeleteMarkType);
        }

        private async Task DeleteMarkType()
        {
            try
            {
                if (SelectedMarkType != null)
                {

                    await markTypesService.DeleteMarkAsync(SelectedMarkType.IdTypeOfMark);
                    MarkTypes.Remove(SelectedMarkType);
                }

            }
            catch (Exception e)
            {

            }
        }

        private async Task AddMarkType()
        {
            await markTypesService.AddMarkTypeAsync(newMarkType);
            isAdding = false;

            MarkTypes.Add(NewMarkType);
        }

        protected MarkTypesContentViewModel()
        {}

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            MarkTypes.Clear();
            MarkTypes.AddRange(await markTypesService.GetMarkTypesAsync());
        }

    }
}