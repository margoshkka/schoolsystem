﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Extensions;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Modules.Content.Constants;
using SchoolSystem.Desktop.Services.Core.Interfaces;


namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class LoginContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private readonly IAuthorizationService authorizationService;
        private bool isAuthorizationError = false;


        public bool IsAuthorizationError
        {
            get { return isAuthorizationError; }
            set
            {
                if (value == isAuthorizationError)
                    return;
                isAuthorizationError = value;
                OnPropertyChanged();
            }
        }

        public ICommand LogInCommand { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public CurrentUser CurrentUser { get; set; }
        #endregion

        #region Constructors   

        public LoginContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IAuthorizationService authorizationService,
            CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.authorizationService = authorizationService;

            LogInCommand= DelegateCommand.FromAsyncHandler(Authorization);
        }

        async Task  Authorization()
        {
            try
            {

                
                await this.authorizationService.LogInAsync(Login, Password);
                IsAuthorizationError = false;
                CurrentUser.Role = await authorizationService.GetRoleAsync(Login);
                CurrentUser.IsLoggedIn = true;
                regionManager.RequestNavigate(RegionNames.ContentRegion, ViewNames.MarksView);
                regionManager.Regions[RegionNames.HubRegion].Clear();

            }
            catch (Exception)
            {
                IsAuthorizationError = true;
            }
            
        }

        // InitializeEvents();
        protected LoginContentViewModel()
        {}

        #endregion

        
        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
        }
    }
}