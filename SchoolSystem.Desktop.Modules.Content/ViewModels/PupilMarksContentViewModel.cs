﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class PupilMarksContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IMarkService markService;
        private Pupil currentPupil;

        public Pupil CurrentPupil
        {
            get { return currentPupil; }
            set
            {
                if (Equals(value, currentPupil))
                    return;
                currentPupil = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Mark> Marks { get; set; }

        #endregion

        #region Constructors   

        public PupilMarksContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager,IMarkService markService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.markService = markService;
            Marks=new ObservableCollection<Mark>();
        }

        protected PupilMarksContentViewModel()
        {}

        #endregion
        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Pupil pupil = navigationContext.Parameters[Constants.NavigationParameters.PupilParametr] as Pupil;
            if (pupil == null)
                throw new ArgumentNullException(nameof(pupil));
            CurrentPupil = pupil;
            Marks.Clear();
            Marks.AddRange((await markService.GetMarksAsync()).Where(mark => mark.PupilId == CurrentPupil.IdPupil));

        }
    }
}