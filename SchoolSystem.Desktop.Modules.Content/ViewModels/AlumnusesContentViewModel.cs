﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class AlumnusesContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private readonly IAlumnusService alumnusService;
        private Alumnus selectedAlumnus;
        private bool isUpdating;
        private BasicAlumnusInfo editAlumnusInfo;

        public ICommand UpdateCommand { get; set; }
        public ICommand UpdateAlumnusCommand { get; set; }
        public CurrentUser CurrentUser { get; set; }

        public BasicAlumnusInfo EditAlumnusInfo
        {
            get { return editAlumnusInfo; }
            set
            {
                if (Equals(value, editAlumnusInfo))
                    return;
                editAlumnusInfo = value;
                OnPropertyChanged();
            }
        }

        public bool IsUpdating
        {
            get { return isUpdating; }
            set
            {
                if (value == isUpdating)
                    return;
                isUpdating = value;
                OnPropertyChanged();
            }
        }

        public Alumnus SelectedAlumnus
        {
            get { return selectedAlumnus; }
            set
            {
                if (Equals(value, selectedAlumnus))
                    return;
                selectedAlumnus = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Alumnus> Alumnuses { set; get; }
        #endregion

        #region Constructors   

        public AlumnusesContentViewModel(IEventAggregator eventAggregator, 
            IRegionManager regionManager, IAlumnusService alumnusService, CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.alumnusService = alumnusService;

            Alumnuses = new ObservableCollection<Alumnus>();

            UpdateCommand = new DelegateCommand(() =>
            {
                if (SelectedAlumnus != null)
                {
                    EditAlumnusInfo = new BasicAlumnusInfo()
                    {
                        Address = SelectedAlumnus.Address,
                        FormOfEducation = SelectedAlumnus.FormOfEducation,
                        Surname = SelectedAlumnus.Surname,
                        Telephone = SelectedAlumnus.Telephone,
                        YearOfGraduation = SelectedAlumnus.YearOfGraduation,
                        NumberOfClasses = SelectedAlumnus.NumberOfClasses,
                        InstitutionOfEntry = SelectedAlumnus.InstitutionOfEntry

                    };

                    IsUpdating = true;
                }

            });
            UpdateAlumnusCommand = DelegateCommand.FromAsyncHandler(UpdateAlumnus);
        }

        private async Task UpdateAlumnus()
        {
            try
            {

                await alumnusService.UpdateAlumnusAsync(SelectedAlumnus.IdAlumnus, EditAlumnusInfo);
                isUpdating = false;
                SelectedAlumnus.Address = EditAlumnusInfo.Address;
                SelectedAlumnus.FormOfEducation =  EditAlumnusInfo.FormOfEducation;
                SelectedAlumnus.Surname = EditAlumnusInfo.Surname;
                SelectedAlumnus.Telephone = EditAlumnusInfo.Telephone;
                //SelectedAlumnus.NumberOfClasses = EditAlumnusInfo.NumberOfClasses;
                SelectedAlumnus.InstitutionOfEntry = EditAlumnusInfo.InstitutionOfEntry;
            }
            catch (Exception e)
            {
            }
        }

        protected AlumnusesContentViewModel()
        { }
        #endregion

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            Alumnuses.Clear();
            Alumnuses.AddRange( await alumnusService.GetAlumnusesAsync());
        }
    }
}