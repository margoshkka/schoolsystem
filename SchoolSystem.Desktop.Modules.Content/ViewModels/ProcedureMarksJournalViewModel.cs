﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureMarksJournalViewModel : BaseViewModel
    {

        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IPupilsService pupilsService;
        private readonly IMarkService markService;
        private Pupil pupil;
        private bool isIdEnter = false;
        private int? pupilId;
        private int? classId;
        private int? subjectId;


        public Pupil Pupil
        {
            get { return pupil; }
            set
            {
                if (Equals(value, pupil))
                    return;
                pupil = value;
                OnPropertyChanged();
            }
        }

        public int? PupilId
        {
            get { return pupilId; }
            set
            {
                if (value == pupilId)
                    return;
                pupilId = value;
                OnPropertyChanged();
            }
        }

        public int? SubjectId
        {
            get { return subjectId; }
            set
            {
                if (value == subjectId)
                    return;
                subjectId = value;
                OnPropertyChanged();
            }
        }

        public int? ClassId
        {
            get { return classId; }
            set
            {
                if (value == classId)
                    return;
                classId = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Mark> Marks { get; set; }

        public bool IsIdEnter
        {
            get { return isIdEnter; }
            set
            {
                if (value == isIdEnter)
                    return;
                isIdEnter = value;
                OnPropertyChanged();
            }
        }

        public ICommand GetMarksCommand { get; set; }

        #endregion

        #region Constructors   

        public ProcedureMarksJournalViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IPupilsService pupilsService,
                                              IMarkService markService)
        {
            this.markService = markService;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.pupilsService = pupilsService;

            Marks = new ObservableCollection<Mark>();
            GetMarksCommand = DelegateCommand.FromAsyncHandler(GetMarksAsync);
            Pupil = new Pupil();
        }

        private async Task GetMarksAsync()
        {
            try
            {
                Marks.Clear();
                Marks.AddRange(await markService.FilterMarksAsync(PupilId, SubjectId, ClassId));
                //Pupil = await pupilsService.GetPupilAsync(Pupil.IdPupil);
                IsIdEnter = true;
            }
            catch (Exception)
            {
                IsIdEnter = false;
                throw;
            }
        }

        protected ProcedureMarksJournalViewModel()
        {}

        #endregion

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);



        }
    }
}