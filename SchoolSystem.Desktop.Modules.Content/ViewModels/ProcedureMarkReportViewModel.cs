﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureMarkReportViewModel : BaseViewModel
    {

        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IPupilsService pupilsService;
        private readonly IMarkService markService;



   
        public ObservableCollection<Mark> Marks { get; set; }
        public ObservableCollection<Pupil> Pupils { get; set; }


        public ICommand GetMarksCommand { get; set; }

        #endregion

        #region Constructors   

        public ProcedureMarkReportViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IPupilsService pupilsService,
                                              IMarkService markService)
        {
            this.markService = markService;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.pupilsService = pupilsService;

            Marks = new ObservableCollection<Mark>();
            Pupils=new ObservableCollection<Pupil>();
        }


        protected ProcedureMarkReportViewModel()
        { }

        #endregion

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            try
            {
                Marks.Clear();
                Marks.AddRange(await markService.GetTopPupilsAsync());

                
            }
            catch (Exception)
            {
                
                throw;
            }


        }
    }
}