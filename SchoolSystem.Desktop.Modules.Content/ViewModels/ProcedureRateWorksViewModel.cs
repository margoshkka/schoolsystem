﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureRateWorksViewModel : BaseViewModel
    {

        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly ISubjectService subjectService;
        private readonly IMarkService markService;
        private Subject selectedSubject;

        public ObservableCollection<Mark> Marks { get; set; }
        public ObservableCollection<Subject> Subjects { get; set; }

        public Subject SelectedSubject
        {
            get { return selectedSubject; }
            set
            {
                if (Equals(value, selectedSubject))
                    return;
                selectedSubject = value;
                OnPropertyChanged();
                GetMarksAsync();
            }
        }



        public ICommand GetMarksCommand { get; set; }

        #endregion

        #region Constructors   

        public ProcedureRateWorksViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, ISubjectService subjectService,
                                              IMarkService markService)
        {
            this.markService = markService;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.subjectService = subjectService;

            Subjects=new ObservableCollection<Subject>();
            Marks = new ObservableCollection<Mark>();
        }

        private async Task GetMarksAsync()
        {
            try
            {
                Marks.Clear();
                Marks.AddRange(
                    (await markService.GetMarksAsync()).Where(mark => mark.SubjectId == SelectedSubject.IdSubject&&mark.MarkTypeId==1).OrderByDescending(mark => mark.MarkValue));
                //Pupil = await pupilsService.GetPupilAsync(Pupil.IdPupil);
            }
            catch (Exception)
            {
               
            }
        }

        protected ProcedureRateWorksViewModel()
        { }

        #endregion

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            try
            {
                base.OnNavigatedTo(navigationContext);

                Subjects.Clear();
                Subjects.AddRange(await subjectService.GetSubjectsAsync());

            }
            catch (Exception e)
            {}
        }
    }
}