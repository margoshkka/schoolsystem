﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class RegistrationContentViewModel : BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IAuthorizationService authorizationService;
        private bool isAuthorizationOk;
        private bool isAuthorizationError = false;
        public CurrentUser CurrentUser { get; set; }
        public ObservableCollection<string> Roles { get; set; }=new ObservableCollection<string>() {        
        "Guest",
        "Teacher",
        "MainTeacher",
        "Administrator"
    };

        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
        public string Role { get; set; }
        public ICommand RegistrationCommand { get; set; }

        public bool IsAuthorizationError
        {
            get { return isAuthorizationError; }
            set
            {
                if (value == isAuthorizationError)
                    return;
                isAuthorizationError = value;
                OnPropertyChanged();
            }
        }

        public bool IsAuthorizationOk
        {
            get { return isAuthorizationOk; }
            set
            {
                if (value == isAuthorizationOk)
                    return;
                isAuthorizationOk = value;
                OnPropertyChanged();
            }
        }


        public RegistrationContentViewModel(IRegionManager regionManager, IEventAggregator eventAggregator, 
            IAuthorizationService authorizationService, CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;
            this.authorizationService = authorizationService;

            RegistrationCommand = DelegateCommand.FromAsyncHandler(Registration);
        }

        public  async Task Registration()
        {
            try
            {
                
                await authorizationService.RegisterAsync(Email, Password, PasswordConfirm, Role);
                IsAuthorizationOk = true;
            }
            catch (Exception)
            {
                IsAuthorizationOk = false;
                IsAuthorizationError = true;
            }
        }

        protected RegistrationContentViewModel()
        {}

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

        }
    }
}


