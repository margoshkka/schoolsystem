﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using Syncfusion.Windows.Shared;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureInstitutesViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IAlumnusService alumnusService;

        public ProcedureInstitutesViewModel(IEventAggregator eventAggregator, IAlumnusService alumnusService, IRegionManager regionManager)
        {
            this.eventAggregator = eventAggregator;
            this.alumnusService = alumnusService;
            this.regionManager = regionManager;
            Institutes=new ObservableCollection<WrapperInstitutes>();
        }

        protected ProcedureInstitutesViewModel()
        {}

        public ObservableCollection<WrapperInstitutes> Institutes { get; set; }

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            var HashSet=new HashSet<string>((await alumnusService.GetAlumnusesAsync()).Select(alumnus => alumnus.InstitutionOfEntry));
            Institutes.AddRange(HashSet.Select(s => new WrapperInstitutes() {Institute = s}));
        }

        public class WrapperInstitutes
        {
            public WrapperInstitutes()
            {}

            public string Institute { get; set; }
        }
    }
}