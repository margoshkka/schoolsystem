﻿using Prism.Events;
using Prism.Regions;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class AlumnusContactInfoContentViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;



        #endregion

        #region Constructors   

        public AlumnusContactInfoContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
        }
        protected AlumnusContactInfoContentViewModel()
        { }
        #endregion 
    }
}