﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Modules.Content.Constants;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using Syncfusion.Data.Extensions;
using NavigationParameters = Prism.Regions.NavigationParameters;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ClassesContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IClassesService classesService;
        private Class newClass;
        private bool isAdding;


        public ICommand OpenClassCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand AddClassCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public Class NewClass
        {
            get { return newClass; }
            set
            {
                if (Equals(value, newClass))
                    return;
                newClass = value;
                OnPropertyChanged();
            }
        }
        
        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (value == isAdding)
                    return;
                isAdding = value;
                OnPropertyChanged();
            }
        }
        

        public CurrentUser CurrentUser { get; set; }
        public ObservableCollection<Class> Classes { get; set; }


        #endregion

        #region Constructors   

        public ClassesContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IClassesService classesService,
            CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.classesService = classesService;
            Classes=new ObservableCollection<Class>();

            InitializeCommands();
           
        }

        private async Task AddClassAsync()
        {
            NewClass= await classesService.AddClassAsync(NewClass);
            isAdding = false;
            Classes.Add(NewClass);
        }

        private async Task DeleteClass(Class @class)
        {
            try
            {
                if ( @class!= null)
                {

                   await classesService.DeleteClassAsync(@class.IdClass);
                    Classes.Remove(@class);
                }

            }
            catch (Exception e)
            {

            }
        }

        private void InitializeCommands()
        {
            OpenClassCommand = new DelegateCommand<Class>(OpenClass);
            AddCommand = new DelegateCommand(() =>
            {
                NewClass = new Class();
                IsAdding = true;

            });
            DeleteCommand = DelegateCommand<Class>.FromAsyncHandler(DeleteClass);
            AddClassCommand = DelegateCommand.FromAsyncHandler(AddClassAsync);

        }

        protected ClassesContentViewModel()
        {}

        #endregion

        #region Methods

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Classes.Clear();
            var asd = await classesService.GetClassesAsync();
            Classes.AddRange(asd.OrderByDescending(@class => @class.ClassYear).ThenByDescending(@class => @class.Number));
            

        }

        private void OpenClass(Class @class)
        {
            eventAggregator.GetEvent<ContentChangedEvent>().Publish(new ContentChangedEventArgs(ContentItemNames.ClassItem,@class));
            regionManager.RequestNavigate(RegionNames.ContentRegion, new Uri(ViewNames.ClassPupilsView, UriKind.Relative),
                                          new NavigationParameters() {{Constants.NavigationParameters.ClassParametr, @class}});
        }

        #endregion

    }
}