﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureAlumnusWith9ClassesViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IAlumnusService alumnusService;

        public ObservableCollection<Alumnus> Alumnuses { get; set; }
   
        public ProcedureAlumnusWith9ClassesViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IAlumnusService alumnusService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.alumnusService = alumnusService;
            Alumnuses = new ObservableCollection<Alumnus>();
        }

      

        protected ProcedureAlumnusWith9ClassesViewModel()
        { }

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Alumnuses.Clear();
            Alumnuses.AddRange(await alumnusService.GetAlumnusesWith9ClassesAsync());
        }
    
}
}