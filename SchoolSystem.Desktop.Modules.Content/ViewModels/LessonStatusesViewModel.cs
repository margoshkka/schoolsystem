﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class LessonStatusesViewModel : BaseViewModel
    {
        #region Fields

        readonly IRegionManager regionManager;
        readonly IEventAggregator eventAggregator;
        readonly ILessonStatusService lessonStatusService;
        private bool isAdding;
        private LessonStatus selectedLessonStatus;
        private LessonStatus newLessonStatus;

        #endregion

        #region Properties, Indexers

        public ObservableCollection<LessonStatus> LessonStatuses { get; set; }

        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (value == isAdding)
                    return;
                isAdding = value;
                OnPropertyChanged();
            }
        }

        public LessonStatus SelectedLessonStatus
        {
            get { return selectedLessonStatus; }
            set
            {
                if (Equals(value, selectedLessonStatus))
                    return;
                selectedLessonStatus = value;
                OnPropertyChanged();
            }
        }

        public LessonStatus NewLessonStatus
        {
            get { return newLessonStatus; }
            set
            {
                if (Equals(value, newLessonStatus))
                    return;
                newLessonStatus = value;
                OnPropertyChanged();
            }
        }

        public CurrentUser CurrentUser { get; set; }

        public ICommand AddCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand AddLessonStatusCommand { get; set; }

        #endregion

        #region Constructors

        public LessonStatusesViewModel(IRegionManager regionManager, IEventAggregator eventAggregator, ILessonStatusService lessonStatusService,
            CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;
            this.lessonStatusService = lessonStatusService;

            LessonStatuses=new ObservableCollection<LessonStatus>();
            InitializeCommands();
        }

        protected LessonStatusesViewModel()
        {}

        #endregion

        #region Methods

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            LessonStatuses.Clear();
            LessonStatuses.AddRange(await lessonStatusService.GetLessonStatusesAsync());

        }


        private void InitializeCommands()
        {
            AddCommand = new DelegateCommand(() =>
            {
                NewLessonStatus = new LessonStatus();
                IsAdding = true;

            });
            AddLessonStatusCommand = DelegateCommand.FromAsyncHandler(AddLessonStatus);
            DeleteCommand = DelegateCommand.FromAsyncHandler(DeleteLessonStatus);
        }

        private async Task AddLessonStatus()
        {
            await lessonStatusService.AddLessonStatusAsync(NewLessonStatus);
            isAdding = false;

            LessonStatuses.Add(NewLessonStatus);
        }

        private async Task DeleteLessonStatus()
        {
            try
            {
                if (SelectedLessonStatus != null)
                {

                    await lessonStatusService.DeleteLessonStatusAsync(SelectedLessonStatus.IdStatus);
                    LessonStatuses.Remove(SelectedLessonStatus);
                }

            }
            catch (Exception e)
            {

            }
        }

        #endregion
    }
}