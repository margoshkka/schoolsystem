﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ClassTypesContentViewModel:BaseViewModel
    {
        #region Fields

        readonly IRegionManager regionManager;
        readonly IEventAggregator eventAggregator;
        readonly IClassTypesService classTypesService;
        private bool isAdding;
        private ClassType selectedClassType;
        private ClassType newClassType;

        #endregion

        #region Properties, Indexers

        public ObservableCollection<ClassType> ClassTypes { get; set; }

        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (value == isAdding)
                    return;
                isAdding = value;
                OnPropertyChanged();
            }
        }

        public ClassType NewClassType
        {
            get { return newClassType; }
            set
            {
                if (Equals(value, newClassType))
                    return;
                newClassType = value;
                OnPropertyChanged();
            }
        }

        public CurrentUser CurrentUser { get; set; }

        public ClassType SelectedClassType
        {
            get { return selectedClassType; }
            set
            {
                if (Equals(value, selectedClassType))
                    return;
                selectedClassType = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand AddClassTypeCommand { get; set; }
        public ICommand DeleteClassTypeCommand { get; set; }

        #endregion

        #region Constructors

        public ClassTypesContentViewModel(IClassTypesService classTypesService, IEventAggregator eventAggregator, IRegionManager regionManager,
            CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.classTypesService = classTypesService;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            ClassTypes=new ObservableCollection<ClassType>();

            AddCommand = new DelegateCommand(() =>
            {
                NewClassType = new ClassType();
                IsAdding = true;

            });

            AddClassTypeCommand = DelegateCommand.FromAsyncHandler(AddClassType);
            DeleteClassTypeCommand = DelegateCommand.FromAsyncHandler(DeleteClassType);
        }

        protected ClassTypesContentViewModel()
        {}

        #endregion

        #region Methods

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            ClassTypes.Clear();
            ClassTypes.AddRange(await classTypesService.GetClassTypesAsync());

            
        }

        private async Task DeleteClassType()
        {
            try
            {
                if (SelectedClassType != null)
                {

                    await classTypesService.DeleteClassTypeAsync(SelectedClassType.IdTypeOfClass);
                    ClassTypes.Remove(SelectedClassType);
                }

            }
            catch (Exception e)
            {

            }
        }

        private async Task AddClassType()
        {
            await classTypesService.AddClassTypeAsync(NewClassType);
            isAdding = false;

            ClassTypes.Add(NewClassType);
        }

        #endregion
    }
}