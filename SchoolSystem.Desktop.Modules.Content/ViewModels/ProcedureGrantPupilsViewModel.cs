﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using Syncfusion.Windows.Shared;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureGrantPupilsViewModel:BaseViewModel
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IAlumnusService alumnusService;
        private double procentAlumnWithGrant;
        private bool isYearEnter;
        private int? year;

        public ObservableCollection<Alumnus> Alumnuses { get; set; }
        public ICommand GetProcentCommand { get; set; }
        public double ProcentAlumnWithGrant
        {
            get { return procentAlumnWithGrant; }
            set
            {
                if (value == procentAlumnWithGrant)
                    return;
                procentAlumnWithGrant = value;
                OnPropertyChanged();
            }
        }

        public int? Year
        {
            get { return year; }
            set
            {
                if (value == year)
                    return;
                year = value;
                OnPropertyChanged();
            }
        }

        public bool IsYearEnter
        {
            get { return isYearEnter; }
            set
            {
                if (value == isYearEnter)
                    return;
                isYearEnter = value;
                OnPropertyChanged();
            }
        }

        public ProcedureGrantPupilsViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IAlumnusService alumnusService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.alumnusService = alumnusService;
            GetProcentCommand = DelegateCommand.FromAsyncHandler(GetProcentAsync);
            Alumnuses=new ObservableCollection<Alumnus>();
        }

        private async Task GetProcentAsync()
        {
           ProcentAlumnWithGrant= await alumnusService.GetProcentAlumnusesWithGrantAsync(Year);
        }

        protected ProcedureGrantPupilsViewModel()
        {}

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Alumnuses.Clear();
            Alumnuses.AddRange(await alumnusService.GetAlumnusesWithGrantAsync());
        }
    }
}