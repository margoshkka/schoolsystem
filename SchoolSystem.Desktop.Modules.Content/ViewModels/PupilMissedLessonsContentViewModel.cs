﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class PupilMissedLessonsContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly ILessonsService lessonsService;
        private Pupil currentPupil;

        public Pupil CurrentPupil
        {
            get { return currentPupil; }
            set
            {
                if (Equals(value, currentPupil))
                    return;
                currentPupil = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Lesson> MissedPupilsLessons
        {
            get;
            set;
        }
        public ObservableCollection<Lesson> WorkedOutPupilsLessons
        {
            get;
            set;
        }
        #endregion

        #region Constructors   

        public PupilMissedLessonsContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, ILessonsService lessonsService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.lessonsService = lessonsService;
            MissedPupilsLessons=new ObservableCollection<Lesson>();
            WorkedOutPupilsLessons=new ObservableCollection<Lesson>();
        }
        protected PupilMissedLessonsContentViewModel()
        { }
        #endregion

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Pupil pupil = navigationContext.Parameters[Constants.NavigationParameters.PupilParametr] as Pupil;
            if (pupil == null)
                throw new ArgumentNullException(nameof(pupil));
            CurrentPupil = pupil;
            WorkedOutPupilsLessons.Clear();
            MissedPupilsLessons.Clear();
            List<Lesson> variable = (await lessonsService.GetLessonsAsync()).Where(
                lesson => lesson.PupilId == CurrentPupil.IdPupil
                && (lesson.LessonStatus.StatusName == "Missed" || lesson.LessonStatus.StatusName == "Worked out"))
                                                                   .ToList();

            WorkedOutPupilsLessons.AddRange(variable.Where(lesson => lesson.LessonStatus.StatusName == "Worked out"));
            MissedPupilsLessons.AddRange(variable.Where(lesson => lesson.LessonStatus.StatusName == "Missed"));

        }
    }
}