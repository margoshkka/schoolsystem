﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.UserInfo;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ClassJournalContentViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly ILessonsService lessonsService;

        private Class @class;
        private Lesson newLesson;
        private bool isAdding;
        private bool isUpdating;
        private Lesson selectedLesson;
        private BasicLessonInfo editingLesson;

        #endregion

        #region Properties, Indexers

        public ICommand UpdateCommand { get; set; }
        public ICommand AddCommand { get; set; }

        public ICommand UpdateLessonCommand { get; set; }
        public ICommand AddLessonCommand { get; set; }

        public BasicLessonInfo EditingLesson
        {
            get { return editingLesson; }
            set
            {
                if (Equals(value, editingLesson))
                    return;
                editingLesson = value;
                OnPropertyChanged();
            }
        }

        public Lesson SelectedLesson
        {
            get { return selectedLesson; }
            set
            {
                if (Equals(value, selectedLesson))
                    return;
                selectedLesson = value;
                OnPropertyChanged();
            }
        }

        public bool IsUpdating
        {
            get { return isUpdating; }
            set
            {
                if (value == isUpdating)
                    return;
                isUpdating = value;
                OnPropertyChanged();
            }
        }


        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (value == isAdding)
                    return;
                isAdding = value;
                OnPropertyChanged();
            }
        }

        public CurrentUser CurrentUser { get; set; }

        public Class Class
        {
            get { return @class; }
            set
            {
                if (Equals(value, @class))
                    return;
                @class = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Lesson> Lessons { get; set; }

        public Lesson NewLesson
        {
            get { return newLesson; }
            set
            {
                if (Equals(value, newLesson))
                    return;
                newLesson = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        public ClassJournalContentViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, ILessonsService lessonsService,
            CurrentUser currentUser)
        {
            CurrentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.lessonsService = lessonsService;
            Lessons = new ObservableCollection<Lesson>();

            InitializeCommands();
        }

        protected ClassJournalContentViewModel()
        { 
        }

        #endregion

        #region Methods

        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            Class newClass = navigationContext.Parameters[Constants.NavigationParameters.ClassParametr] as Class;
            if (newClass == null)
                throw new ArgumentNullException(nameof(newClass));
            Class = newClass;
            Lessons.Clear();
            Lessons.AddRange((await lessonsService.GetLessonsAsync()).Where(lesson => lesson.Pupil.ClassId==Class.IdClass));

        }

        private void InitializeCommands()
        {
            AddCommand=new DelegateCommand(() =>
                                           {
                                               NewLesson=new Lesson();
                                               IsAdding = true;

                                           });
            UpdateCommand=new DelegateCommand(() =>
                                              {
                                                  EditingLesson=new BasicLessonInfo() {LessonStatusId = SelectedLesson.LessonStatusId};
                                                  IsUpdating = true;
                                                  
                                              });

            AddLessonCommand = DelegateCommand.FromAsyncHandler(AddLesson);
            UpdateLessonCommand = DelegateCommand.FromAsyncHandler(UpdateLesson);
        }

        private async Task UpdateLesson()
        {
            
            try
            {

                await lessonsService.UpdateLessonAsync(SelectedLesson.IdLesson, EditingLesson);
                isUpdating = false;
                SelectedLesson.LessonStatusId = EditingLesson.LessonStatusId;
            }
            catch (Exception)
            {
                
            }
            
        }


        private async Task AddLesson()
        {
            await lessonsService.AddLessonAsync(NewLesson);
            isAdding = false;
            Lessons.Add(NewLesson);
        }

        #endregion
    }
}