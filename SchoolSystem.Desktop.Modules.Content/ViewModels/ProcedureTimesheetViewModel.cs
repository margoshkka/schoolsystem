﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using Syncfusion.Windows.Shared;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace SchoolSystem.Desktop.Modules.Content.ViewModels
{
    public class ProcedureTimesheetViewModel : BaseViewModel
    {

        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private readonly IPupilsService pupilsService;
        private Pupil pupil;
        private bool isIdEnter = false;
        private readonly IMarkService markService;

        public Pupil Pupil
        {
            get { return pupil; }
            set
            {
                if (Equals(value, pupil))
                    return;
                pupil = value;
                OnPropertyChanged();
            }
        }

        public bool IsIdEnter
        {
            get { return isIdEnter; }
            set
            {
                if (value == isIdEnter)
                    return;
                isIdEnter = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<Mark> Marks { get; set; }
        public ICommand GetTimesheetCommand { get; set; }

        #endregion

        #region Constructors   

        public ProcedureTimesheetViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IPupilsService pupilsService,IMarkService markService
            )
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.pupilsService = pupilsService;
            this.markService = markService;

            GetTimesheetCommand = DelegateCommand.FromAsyncHandler(GetTimesheetAsync);
            Pupil = new Pupil();
            Marks=new ObservableCollection<Mark>();
        }

        private async Task GetTimesheetAsync()
        {
            try
            {
                Pupil = await pupilsService.GetPupilAsync(Pupil.IdPupil);
                Marks.Clear();
                Marks.AddRange((await markService.GetMarksAsync()).Where(
                    mark =>
                    mark.PupilId == Pupil.IdPupil && mark.MarkType.Name == "Year mark"&& mark.Date?.Year==DateTime.UtcNow.Year));


            IsIdEnter = true;
            }
            catch (Exception)
            {
                IsIdEnter = false;
                throw;
            }
        }

        protected ProcedureTimesheetViewModel()
        { }
        #endregion

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);



        }
    }
}