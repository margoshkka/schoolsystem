﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface ILessonsService
    {
        Task AddLessonAsync(Lesson lesson);
        Task<List<Lesson>> GetLessonsAsync();
        Task<Lesson> GetLessonAsync(int lessonId);

        Task UpdateLessonAsync(int lessonId,  BasicLessonInfo lessonInfo);

    }
}