﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface ISubjectService
    {
        Task AddSubject(Subject newSubject);
        Task<List<Subject>> GetSubjectsAsync();
        Task<Subject> GetSubjectAsync(int idSubject);
    }
}