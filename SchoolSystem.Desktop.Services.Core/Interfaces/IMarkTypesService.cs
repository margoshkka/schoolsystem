﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IMarkTypesService
    {
        Task AddMarkTypeAsync(MarkType markType);
        Task<List<MarkType>> GetMarkTypesAsync();
        Task DeleteMarkAsync(int typeMarkId) ;
    }
}