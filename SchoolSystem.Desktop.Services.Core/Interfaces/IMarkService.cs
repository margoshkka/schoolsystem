﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IMarkService
    {
        Task<Mark> AddMarkAsync(Mark mark);
        Task<List<Mark>> GetMarksAsync();
        Task<Mark> GetMarkAsync(int markId);
        Task<List<Mark>> FilterMarksAsync(int? pupilId, int? subjectId, int? classId);
        Task<List<Mark>> GetTopPupilsAsync();
    }
}