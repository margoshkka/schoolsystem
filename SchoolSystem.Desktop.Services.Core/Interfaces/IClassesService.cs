﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IClassesService
    {
        Task<Class> AddClassAsync(Class newClass);
        Task<List<Class>> GetClassesAsync();
        Task<Class> GetClassAsync(int idClass);
        Task DeleteClassAsync(int idClass);
    }
}