﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IPupilsService
    {
        Task<List<Pupil>> GetPupilsAsync();
        Task<Pupil> GetPupilAsync(int pupilId);
        Task UpdatePupilAsync(int pupilId,BasicPupilInfo pupilInfo);
        Task DeletePupilAsync(int pupilId);
        Task<Pupil> AddPupilAsync(Pupil pupil);
        
    }
}