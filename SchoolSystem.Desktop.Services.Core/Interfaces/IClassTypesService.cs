﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IClassTypesService
    {
        Task AddClassTypeAsync(ClassType newClassType);
        Task<List<ClassType>> GetClassTypesAsync();
        Task DeleteClassTypeAsync(int classTypeId);
    }
}