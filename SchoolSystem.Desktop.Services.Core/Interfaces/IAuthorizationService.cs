﻿using System.Threading.Tasks;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IAuthorizationService
    {
        Task LogInAsync(string login, string password);
        Task RegisterAsync(string email, string password, string confirmPassword, string role);

        Task<string> GetRoleAsync(string email);
    }
}