﻿using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IPermissionsService
    {
        Task<AccessToken> GetAccessTokenAsync();
        Task SetAccessTokenAsync(AccessToken accessToken);
    }
}