﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface IParentsService
    {
        Task<Parent> GetParentAsync(int parentId);
        Task UpdateParentAsync(int parentId, BasicParentInfo parentInfo);
        Task AddParentAsync(Parent parent);
    }
}