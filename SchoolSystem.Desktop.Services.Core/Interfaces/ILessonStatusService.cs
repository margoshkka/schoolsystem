﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Interfaces
{
    public interface ILessonStatusService
    {
        Task<LessonStatus> AddLessonStatusAsync(LessonStatus lessonStatus);
        Task DeleteLessonStatusAsync(int idLessonStatus);
        Task<List<LessonStatus>> GetLessonStatusesAsync();
    }
}