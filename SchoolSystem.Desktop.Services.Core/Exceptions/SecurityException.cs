﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace SchoolSystem.Desktop.Services.Core.Exceptions
{
    public class SecurityException : ApiException
    {
        public SecurityException()
        { }

        public SecurityException(string message) : base(message)
        { }

        public SecurityException(string message, Exception innerException) : base(message, innerException)
        { }

        protected SecurityException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}