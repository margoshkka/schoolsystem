﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace SchoolSystem.Desktop.Services.Core.Exceptions
{
    public class ApiException:Exception
    {
        public ApiException()
        { }

        public ApiException(string message) : base(message)
        { }

        public ApiException(string message, Exception innerException) : base(message, innerException)
        { }

        protected ApiException([NotNull]SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}