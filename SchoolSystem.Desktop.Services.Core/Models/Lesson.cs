   using System;
    using System.Collections.Generic;
namespace SchoolSystem.Desktop.Services.Core.Models
{
 
    
    public  class Lesson:BindableModel
    {
        private int idLesson;
        private int pupilId;
        private int subjectId;
        private DateTime? date;
        private int lessonStatusId;
        private LessonStatus lessonStatus;
        private Pupil pupil;
        private Subject subject;

        public int IdLesson
        {
            get { return idLesson; }
            set
            {
                if (value == idLesson)
                    return;
                idLesson = value;
                OnPropertyChanged();
            }
        }

        public int PupilId
        {
            get { return pupilId; }
            set
            {
                if (value == pupilId)
                    return;
                pupilId = value;
                OnPropertyChanged();
            }
        }

        public int SubjectId
        {
            get { return subjectId; }
            set
            {
                if (value == subjectId)
                    return;
                subjectId = value;
                OnPropertyChanged();
            }
        }

        public Nullable<System.DateTime> Date
        {
            get { return date; }
            set
            {
                if (value.Equals(date))
                    return;
                date = value;
                OnPropertyChanged();
            }
        }

        public int LessonStatusId
        {
            get { return lessonStatusId; }
            set
            {
                if (value == lessonStatusId)
                    return;
                lessonStatusId = value;
                OnPropertyChanged();
            }
        }

        public virtual LessonStatus LessonStatus
        {
            get { return lessonStatus; }
            set
            {
                if (Equals(value, lessonStatus))
                    return;
                lessonStatus = value;
                OnPropertyChanged();
            }
        }

        public virtual Pupil Pupil
        {
            get { return pupil; }
            set
            {
                if (Equals(value, pupil))
                    return;
                pupil = value;
                OnPropertyChanged();
            }
        }

        public virtual Subject Subject
        {
            get { return subject; }
            set
            {
                if (Equals(value, subject))
                    return;
                subject = value;
                OnPropertyChanged();
            }
        }
    }
}
