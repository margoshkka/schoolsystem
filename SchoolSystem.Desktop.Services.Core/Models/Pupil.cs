using System;
    using System.Collections.Generic;
namespace SchoolSystem.Desktop.Services.Core.Models
{
    
    
    public  class Pupil:BindableModel
    {
        private int idPupil;
        private string name;
        private string surname;
        private string secondName;
        private DateTime? dateOfBirth;
        private string telephone;
        private string address;
        private int? yearOfStartEducation;
        private int classId;
        private int fatherId;
        private int motherId;
        private Parent parent;
        private Parent parent1;
        private Class @class;

        public int IdPupil
        {
            get { return idPupil; }
            set
            {
                if (value == idPupil)
                    return;
                idPupil = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                    return;
                name = value;
                OnPropertyChanged();
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                if (value == surname)
                    return;
                surname = value;
                OnPropertyChanged();
            }
        }

        public string SecondName
        {
            get { return secondName; }
            set
            {
                if (value == secondName)
                    return;
                secondName = value;
                OnPropertyChanged();
            }
        }

        public Nullable<System.DateTime> DateOfBirth
        {
            get { return dateOfBirth; }
            set
            {
                if (value.Equals(dateOfBirth))
                    return;
                dateOfBirth = value;
                OnPropertyChanged();
            }
        }

        public string Telephone
        {
            get { return telephone; }
            set
            {
                if (value == telephone)
                    return;
                telephone = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                if (value == address)
                    return;
                address = value;
                OnPropertyChanged();
            }
        }

        public Nullable<int> YearOfStartEducation
        {
            get { return yearOfStartEducation; }
            set
            {
                if (value == yearOfStartEducation)
                    return;
                yearOfStartEducation = value;
                OnPropertyChanged();
            }
        }

        public int ClassId
        {
            get { return classId; }
            set
            {
                if (value == classId)
                    return;
                classId = value;
                OnPropertyChanged();
            }
        }

        public int FatherId
        {
            get { return fatherId; }
            set
            {
                if (value == fatherId)
                    return;
                fatherId = value;
                OnPropertyChanged();
            }
        }

        public int MotherId
        {
            get { return motherId; }
            set
            {
                if (value == motherId)
                    return;
                motherId = value;
                OnPropertyChanged();
            }
        }

        public virtual Parent Parent
        {
            get { return parent; }
            set
            {
                if (Equals(value, parent))
                    return;
                parent = value;
                OnPropertyChanged();
            }
        }

        public virtual Parent Parent1
        {
            get { return parent1; }
            set
            {
                if (Equals(value, parent1))
                    return;
                parent1 = value;
                OnPropertyChanged();
            }
        }

        public virtual Class Class
        {
            get { return @class; }
            set
            {
                if (Equals(value, @class))
                    return;
                @class = value;
                OnPropertyChanged();
            }
        }
    }
}
