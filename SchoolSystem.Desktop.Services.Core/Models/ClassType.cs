
    using System;
    using System.Collections.Generic;
namespace SchoolSystem.Desktop.Services.Core.Models
{

    
    public  class ClassType:BindableModel
    {
        private int idTypeOfClass;
        private string name;

        public int IdTypeOfClass
        {
            get { return idTypeOfClass; }
            set
            {
                if (value == idTypeOfClass)
                    return;
                idTypeOfClass = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                    return;
                name = value;
                OnPropertyChanged();
            }
        }
    }
}
