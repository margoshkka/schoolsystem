﻿namespace SchoolSystem.Desktop.Services.Core.Models
{
    public class BasicParentInfo
    {
        public string Telephone { get; set; }
        public string Job { get; set; }
    }
}