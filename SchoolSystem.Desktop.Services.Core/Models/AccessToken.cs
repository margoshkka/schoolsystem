﻿using System;

namespace SchoolSystem.Desktop.Services.Core.Models
{
    public class AccessToken
    {
        public string Token { get; private set; }
        public DateTime ExpirationTime { get; private set; }
        public string TokenType { get; private set; }

        public string RefreshToken { get; private set; }

        public string UserName { get; private set; }

        public AccessToken()
        {
        }

        public AccessToken(string token, string refreshToken, string tokenType, string userName, DateTime expirationTime)
        {
            Token = token;
            RefreshToken = refreshToken;
            TokenType = tokenType;
            UserName = userName;
            ExpirationTime = expirationTime;
        }

        public bool IsValid => ExpirationTime > DateTime.Now;

        public override string ToString()
        {
            return TokenType + " " + Token;
        }
    }
}