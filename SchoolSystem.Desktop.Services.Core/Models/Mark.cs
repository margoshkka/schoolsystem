 using System;
    using System.Collections.Generic;

namespace SchoolSystem.Desktop.Services.Core.Models
{
   
    
    public  class Mark:BindableModel
    {
        private int idMark;
        private int pupilId;
        private int subjectId;
        private DateTime? date;
        private int? markValue;
        private int markTypeId;
        private MarkType markType;
        private Pupil pupil;
        private Subject subject;

        public int IdMark
        {
            get { return idMark; }
            set
            {
                if (value == idMark)
                    return;
                idMark = value;
                OnPropertyChanged();
            }
        }

        public int PupilId
        {
            get { return pupilId; }
            set
            {
                if (value == pupilId)
                    return;
                pupilId = value;
                OnPropertyChanged();
            }
        }

        public int SubjectId
        {
            get { return subjectId; }
            set
            {
                if (value == subjectId)
                    return;
                subjectId = value;
                OnPropertyChanged();
            }
        }

        public Nullable<System.DateTime> Date
        {
            get { return date; }
            set
            {
                if (value.Equals(date))
                    return;
                date = value;
                OnPropertyChanged();
            }
        }

        public Nullable<int> MarkValue
        {
            get { return markValue; }
            set
            {
                if (value == markValue)
                    return;
                markValue = value;
                OnPropertyChanged();
            }
        }

        public int MarkTypeId
        {
            get { return markTypeId; }
            set
            {
                if (value == markTypeId)
                    return;
                markTypeId = value;
                OnPropertyChanged();
            }
        }

        public virtual MarkType MarkType
        {
            get { return markType; }
            set
            {
                if (Equals(value, markType))
                    return;
                markType = value;
                OnPropertyChanged();
            }
        }

        public virtual Pupil Pupil
        {
            get { return pupil; }
            set
            {
                if (Equals(value, pupil))
                    return;
                pupil = value;
                OnPropertyChanged();
            }
        }

        public virtual Subject Subject
        {
            get { return subject; }
            set
            {
                if (Equals(value, subject))
                    return;
                subject = value;
                OnPropertyChanged();
            }
        }
    }
}
