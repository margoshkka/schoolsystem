    using System;
    using System.Collections.Generic;
namespace SchoolSystem.Desktop.Services.Core.Models
{

    
    public  class Subject:BindableModel
    {
        private int idSubject;
        private string name;


        public int IdSubject
        {
            get { return idSubject; }
            set
            {
                if (value == idSubject)
                    return;
                idSubject = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                    return;
                name = value;
                OnPropertyChanged();
            }
        }
    }
}
