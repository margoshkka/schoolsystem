﻿namespace SchoolSystem.Desktop.Services.Core.Models
{
    public class BasicAlumnusInfo
    {
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public int? YearOfGraduation { get; set; }
        public string InstitutionOfEntry { get; set; }
        public int? NumberOfClasses { get; set; }
        public string FormOfEducation { get; set; }
    }
}