  using System;
    using System.Collections.Generic;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Services.Core.Models
{
  
    
    public class Class:BindableModel
    {
        private int idClass;
        private string letter;
        private int number;
        private int classTypesId;
        private int classYear;
        private ClassType classType;

        public int IdClass
        {
            get { return idClass; }
            set
            {
                if (value == idClass)
                    return;
                idClass = value;
                OnPropertyChanged();
            }
        }

        public string Letter
        {
            get { return letter; }
            set
            {
                if (value == letter)
                    return;
                letter = value;
                OnPropertyChanged();
            }
        }

        public int Number
        {
            get { return number; }
            set
            {
                if (value == number)
                    return;
                number = value;
                OnPropertyChanged();
            }
        }

        public int ClassTypesId
        {
            get { return classTypesId; }
            set
            {
                if (value == classTypesId)
                    return;
                classTypesId = value;
                OnPropertyChanged();
            }
        }

        public int ClassYear
        {
            get { return classYear; }
            set
            {
                if (value == classYear)
                    return;
                classYear = value;
                OnPropertyChanged();
            }
        }

        public virtual ClassType ClassType
        {
            get { return classType; }
            set
            {
                if (Equals(value, classType))
                    return;
                classType = value;
                OnPropertyChanged();
            }
        }
    }
}
