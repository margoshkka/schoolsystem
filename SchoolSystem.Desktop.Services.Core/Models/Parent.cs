    using System;
    using System.Collections.Generic;
namespace SchoolSystem.Desktop.Services.Core.Models
{

    
    public  class Parent:BindableModel
    {
        private int idParent;
        private string name;
        private string surname;
        private string secondName;
        private string telephone;
        private string sex;
        private string job;


        public int IdParent
        {
            get { return idParent; }
            set
            {
                if (value == idParent)
                    return;
                idParent = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                    return;
                name = value;
                OnPropertyChanged();
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                if (value == surname)
                    return;
                surname = value;
                OnPropertyChanged();
            }
        }

        public string SecondName
        {
            get { return secondName; }
            set
            {
                if (value == secondName)
                    return;
                secondName = value;
                OnPropertyChanged();
            }
        }

        public string Telephone
        {
            get { return telephone; }
            set
            {
                if (value == telephone)
                    return;
                telephone = value;
                OnPropertyChanged();
            }
        }

        public string Sex
        {
            get { return sex; }
            set
            {
                if (value == sex)
                    return;
                sex = value;
                OnPropertyChanged();
            }
        }

        public string Job
        {
            get { return job; }
            set
            {
                if (value == job)
                    return;
                job = value;
                OnPropertyChanged();
            }
        }
    }
}
