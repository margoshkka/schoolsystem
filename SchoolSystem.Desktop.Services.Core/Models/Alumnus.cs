using System;

namespace SchoolSystem.Desktop.Services.Core.Models
{
    public class Alumnus:BindableModel
    {
        private int idAlumnus;
        private string name;//
        private string surname;//
        private string secondName;//
        private DateTime? dateOfBirth;//
        private string telephone;
        private string address;
        private int? yearOfGraduation;//
        private string institutionOfEntry;//
        private int? numberOfClasses;//
        private string formOfEducation;

        public int IdAlumnus
        {
            get { return idAlumnus; }
            set
            {
                if (value == idAlumnus)
                    return;
                idAlumnus = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                    return;
                name = value;
                OnPropertyChanged();
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                if (value == surname)
                    return;
                surname = value;
                OnPropertyChanged();
            }
        }

        public string SecondName
        {
            get { return secondName; }
            set
            {
                if (value == secondName)
                    return;
                secondName = value;
                OnPropertyChanged();
            }
        }

        public Nullable<System.DateTime> DateOfBirth
        {
            get { return dateOfBirth; }
            set
            {
                if (value.Equals(dateOfBirth))
                    return;
                dateOfBirth = value;
                OnPropertyChanged();
            }
        }

        public string Telephone
        {
            get { return telephone; }
            set
            {
                if (value == telephone)
                    return;
                telephone = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                if (value == address)
                    return;
                address = value;
                OnPropertyChanged();
            }
        }

        public Nullable<int> YearOfGraduation
        {
            get { return yearOfGraduation; }
            set
            {
                if (value == yearOfGraduation)
                    return;
                yearOfGraduation = value;
                OnPropertyChanged();
            }
        }

        public string InstitutionOfEntry
        {
            get { return institutionOfEntry; }
            set
            {
                if (value == institutionOfEntry)
                    return;
                institutionOfEntry = value;
                OnPropertyChanged();
            }
        }

        public Nullable<int> NumberOfClasses
        {
            get { return numberOfClasses; }
            set
            {
                if (value == numberOfClasses)
                    return;
                numberOfClasses = value;
                OnPropertyChanged();
            }
        }

        public string FormOfEducation
        {
            get { return formOfEducation; }
            set
            {
                if (value == formOfEducation)
                    return;
                formOfEducation = value;
                OnPropertyChanged();
            }
        }
    }
}
