
 using System;
    using System.Collections.Generic;
namespace SchoolSystem.Desktop.Services.Core.Models
{
   
    
    public  class MarkType:BindableModel
    {
        private int idTypeOfMark;
        private string name;

        public int IdTypeOfMark
        {
            get { return idTypeOfMark; }
            set
            {
                if (value == idTypeOfMark)
                    return;
                idTypeOfMark = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                    return;
                name = value;
                OnPropertyChanged();
            }
        }
    }
}
