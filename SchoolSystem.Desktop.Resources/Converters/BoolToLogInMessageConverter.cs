﻿using System;
using System.Globalization;

namespace SchoolSystem.Desktop.Resources.Converters
{
    public class BoolToLogInMessageConverter:ValueConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isLogged = (bool) value;
            return isLogged ? "Log out" : "Log in";
        }
    }
}