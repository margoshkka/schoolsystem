﻿using System;
using System.Windows.Markup;

namespace SchoolSystem.Desktop.Resources.Converters
{
    public abstract class ConverterBase : MarkupExtension
    {
        #region Methods

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        #endregion
    }
}