﻿using System;
using System.Globalization;
using System.Windows;
using SchoolSystem.Desktop.Infrastructure.Constants;

namespace SchoolSystem.Desktop.Resources.Converters
{
    public class MainTeacherToVisible:ValueConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)value;

            return (str ==Roles.MainTeacher)|| (str == Roles.Administrator) ? Visibility.Visible : Visibility.Hidden;
        }
    }
}