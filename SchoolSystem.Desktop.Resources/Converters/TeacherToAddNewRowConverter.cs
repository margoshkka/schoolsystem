﻿using System;
using System.Globalization;
using System.Windows;
using Syncfusion.UI.Xaml.Grid;
using SchoolSystem.Desktop.Infrastructure.Constants;

namespace SchoolSystem.Desktop.Resources.Converters
{
    public class TeacherToAddNewRowConverter : ValueConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)value;

            return (str == Roles.Teacher) || (str == Roles.MainTeacher) || (str == Roles.Administrator)? AddNewRowPosition.Bottom
                 :AddNewRowPosition.None;
        }
    }
}