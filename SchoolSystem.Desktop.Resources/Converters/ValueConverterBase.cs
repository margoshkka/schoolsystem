using System;
using System.Globalization;
using System.Windows.Data;

namespace SchoolSystem.Desktop.Resources.Converters
{
    public abstract class ValueConverterBase : ConverterBase, IValueConverter
    {
        #region IValueConverter Members

        public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}