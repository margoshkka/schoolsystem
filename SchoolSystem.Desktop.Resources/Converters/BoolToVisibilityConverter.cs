﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SchoolSystem.Desktop.Resources.Converters
{
    [ValueConversion(typeof (bool), typeof (Visibility))]
    public class BoolToVisibilityConverter : ValueConverterBase
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolean = (bool) value;
            return boolean ? Visibility.Visible : Visibility.Hidden;
        }
    }
}