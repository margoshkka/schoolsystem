﻿using System.Windows;
using System.Windows.Controls;

namespace SchoolSystem.Desktop.Resources.Extensions
{
    public class WatermarkTextExtention : DependencyObject
    {
        #region Static Fields and Constants

        public static readonly DependencyProperty IsMonitoringProperty =
            DependencyProperty.RegisterAttached("IsMonitoring", typeof(bool), typeof(WatermarkTextExtention), new UIPropertyMetadata(false, OnIsMonitoringChanged));

        public static readonly DependencyProperty WatermarkTextProperty =
            DependencyProperty.RegisterAttached("WatermarkText", typeof(string), typeof(WatermarkTextExtention), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty TextLengthProperty =
            DependencyProperty.RegisterAttached("TextLength", typeof(int), typeof(WatermarkTextExtention), new UIPropertyMetadata(0));

        private static readonly DependencyProperty HasTextProperty =
            DependencyProperty.RegisterAttached("HasText", typeof(bool), typeof(WatermarkTextExtention), new FrameworkPropertyMetadata(false));

        #endregion

        #region Properties, Indexers

        public bool HasText
        {
            get { return (bool)GetValue(HasTextProperty); }
            set { SetValue(HasTextProperty, value); }
        }

        #endregion

        #region Methods

        public static bool GetIsMonitoring(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonitoringProperty);
        }

        public static void SetIsMonitoring(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonitoringProperty, value);
        }

        public static bool GetWatermarkText(DependencyObject obj)
        {
            return (bool)obj.GetValue(WatermarkTextProperty);
        }

        public static void SetWatermarkText(DependencyObject obj, string value)
        {
            obj.SetValue(WatermarkTextProperty, value);
        }

        public static int GetTextLength(DependencyObject obj)
        {
            return (int)obj.GetValue(TextLengthProperty);
        }

        public static void SetTextLength(DependencyObject obj, int value)
        {
            obj.SetValue(TextLengthProperty, value);

            obj.SetValue(HasTextProperty, value >= 1);
        }

        static void OnIsMonitoringChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var txtBox = d as TextBox;
            if (txtBox != null)
            {
                if ((bool) e.NewValue)
                {
                    txtBox.SetValue(HasTextProperty, txtBox.Text != null && txtBox.Text.Length >= 1);
                    txtBox.TextChanged += TextChanged;
                }
                else
                    txtBox.TextChanged -= TextChanged;
            }
            else
            {
                var passBox = d as PasswordBox;
                if (passBox != null)
                {
                    if ((bool)e.NewValue)
                        passBox.PasswordChanged += PasswordChanged;
                    else
                        passBox.PasswordChanged -= PasswordChanged;
                }
            }
        }

        static void TextChanged(object sender, TextChangedEventArgs e)
        {
            var txtBox = sender as TextBox;
            if (txtBox == null) return;
            SetTextLength(txtBox, txtBox.Text.Length);
        }

        static void PasswordChanged(object sender, RoutedEventArgs e)
        {
            var passBox = sender as PasswordBox;
            if (passBox == null) return;
            SetTextLength(passBox, passBox.Password.Length);
        }

        #endregion
    }
}
