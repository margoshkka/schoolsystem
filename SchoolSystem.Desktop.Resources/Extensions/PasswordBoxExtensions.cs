﻿using System.Windows;
using System.Windows.Controls;

namespace SchoolSystem.Desktop.Resources.Extensions
{
    public static class PasswordBoxExtensions
    {
        #region Password

        public static readonly DependencyProperty PasswordProperty = DependencyProperty.RegisterAttached(
            "Password", typeof (string), typeof (PasswordBoxExtensions), new PropertyMetadata(default(string), PasswordChangedCallback));

        private static void PasswordChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var passwordBox = (PasswordBox) dependencyObject;

            var value = dependencyPropertyChangedEventArgs.NewValue as string;
            if (value != passwordBox.Password)
            {
                passwordBox.Password = value;
            }
        }

        public static void SetPassword(DependencyObject element, string value)
        {
            element.SetValue(PasswordProperty, value);
        }

        public static string GetPassword(DependencyObject element)
        {
            return (string) element.GetValue(PasswordProperty);
        }

        #endregion

        #region IsEnabled

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.RegisterAttached(
            "IsEnabled", typeof(bool), typeof(PasswordBoxExtensions), new PropertyMetadata(default(bool), IsEnabledChangedCallback));

        private static void IsEnabledChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var isEnabled = (bool) dependencyPropertyChangedEventArgs.NewValue;

            var passwordBox = (PasswordBox) dependencyObject;
            if (isEnabled)
            {
                passwordBox.PasswordChanged += OnPasswordChanged;
            }
            else
            {
                passwordBox.PasswordChanged -= OnPasswordChanged;
            }
        }

        private static void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var passwordBox = (PasswordBox) sender;
            passwordBox.SetValue(PasswordProperty, passwordBox.Password);
        }

        public static void SetIsEnabled(DependencyObject element, bool value)
        {
            element.SetValue(IsEnabledProperty, value);
        }

        public static bool GetIsEnabled(DependencyObject element)
        {
            return (bool) element.GetValue(IsEnabledProperty);
        }



        #endregion
    }
}
