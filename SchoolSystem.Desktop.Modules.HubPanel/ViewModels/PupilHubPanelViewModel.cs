﻿using System;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.HubPanel.ViewModels
{
    public class PupilHubPanelViewModel:BaseViewModel
    {
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private bool isPupilContactInfoSelected;
        private bool isPupilMarksSelected;
        private bool isPupilMissedLessonsSelected;

        private Pupil pupil;

        #endregion

        #region Properties, Indexers

        public bool IsPupilMarksSelected
        {
            get { return isPupilMarksSelected; }
            set
            {
                isPupilMarksSelected = value;
                OnPropertyChanged();
                if (value)
                {
                    PublishSectionChange(HubItemNames.PupilMarksButton);
                }
            }
        }

        public bool IsPupilMissedLessonsSelected
        {
            get { return isPupilMissedLessonsSelected; }
            set
            {
                isPupilMissedLessonsSelected = value;
                OnPropertyChanged();
                if (value)
                {
                    PublishSectionChange(HubItemNames.PupilMissedLessonsButton);
                }
            }
        }

        public bool IsPupilContactInfoSelected
        {
            get { return isPupilContactInfoSelected; }
            set
            {
                isPupilContactInfoSelected = value;
                OnPropertyChanged();
                if (value)
                {
                    PublishSectionChange(HubItemNames.PupilContactInfoButton);
                }
            }
        }

        #endregion

        #region Constructors

        public PupilHubPanelViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;

           // InitializeEvents();
        }

        protected PupilHubPanelViewModel()
        { }

        #endregion

        #region Methods

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            var pupil= navigationContext.Parameters[Content.Constants.NavigationParameters.PupilParametr] as Pupil;
            if (pupil == null)
                throw new ArgumentNullException(nameof(pupil));

            this.pupil = pupil;
        }

        private void PublishSectionChange(string viewName)
        {
            eventAggregator.GetEvent<HubPanelChangedEvent>().Publish(new HubPanelChangedEventArgs(viewName, pupil));
        }

        #endregion
    }
}