﻿using System;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.HubPanel.ViewModels
{
    public class ClassHubPanelViewModel:BaseViewModel
    {
        #region Constructors
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private bool isClassPupilsSelected;
        private bool isClassJournalSelected;
        private Class @class;

        public bool IsClassPupilsSelected
        {
            get { return isClassPupilsSelected; }
            set
            {
                isClassPupilsSelected = value;
                OnPropertyChanged();
                if (value)
                {
                    PublishSectionChange(HubItemNames.ClassPupilsButton);
                }
            }
        }

        public bool IsClassJournalSelected
        {
            get { return isClassJournalSelected; }
            set
            {
                isClassJournalSelected = value;
                OnPropertyChanged();
                if (value)
                {
                    PublishSectionChange(HubItemNames.ClassJournalButton);
                }
            }
        }

        #endregion

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            var @class = navigationContext.Parameters[SchoolSystem.Desktop.Modules.Content.Constants.NavigationParameters.ClassParametr] as Class;
            if (@class == null)
                throw new ArgumentNullException(nameof(@class));

            this.@class = @class;
        }
        private void PublishSectionChange(string viewName)
        {
            eventAggregator.GetEvent<HubPanelChangedEvent>().Publish(new HubPanelChangedEventArgs(viewName,@class));
        }

        public ClassHubPanelViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;

            // InitializeEvents();
        }

        protected ClassHubPanelViewModel()
        { }

        #endregion

    }
}