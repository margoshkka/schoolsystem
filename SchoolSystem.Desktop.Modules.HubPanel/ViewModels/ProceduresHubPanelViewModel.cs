﻿using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Infrastructure.UserInfo;

namespace SchoolSystem.Desktop.Modules.HubPanel.ViewModels
{
    public class ProceduresHubPanelViewModel:BaseViewModel
    {
         public CurrentUser CurrentUser { get; set; }
        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        private bool isPupilPersonalDealSelected;
        private bool isTimesheetSelected;
        private bool isMarksJournalSelected;
        private bool isMarkReportSelected;
        private bool isRateCompetitionWorksSelected;
        private bool isInstitutesSelected ;
        private bool isGrantPupilsSelected;
        private bool isAlumnusWith9ClassesSelected;

        public bool IsPupilPersonalDealSelected
        {
            get { return isPupilPersonalDealSelected; }
            set
            {
                isPupilPersonalDealSelected = value;
                OnPropertyChanged();
                if (value)
                {
                    PublishSectionChange(HubItemNames.PupilPersonalDealButton);
                }
            }
        }

        public bool IsTimesheetSelected
        {
            get { return isTimesheetSelected; }
            set
            {
                isTimesheetSelected = value;
                OnPropertyChanged();
                if (value )
                    PublishSectionChange(HubItemNames.TimesheetButton);
            }
        }

        public bool IsMarksJournalSelected
        {
            get { return isMarksJournalSelected; }
            set
            {
                if (value )
                    PublishSectionChange(HubItemNames.MarksJournalButton);
                isMarksJournalSelected = value;
                OnPropertyChanged();
            }
        }

        public bool IsMarkReportSelected
        {
            get { return isMarkReportSelected; }
            set
            {
                isMarkReportSelected = value;
                OnPropertyChanged();
                if (value )
                    PublishSectionChange(HubItemNames.MarkReportButton);
            }
        }

        public bool IsRateCompetitionWorksSelected
        {
            get { return isRateCompetitionWorksSelected; }
            set
            {
                isRateCompetitionWorksSelected = value;
                OnPropertyChanged();
                if (value )
                    PublishSectionChange(HubItemNames.RateCompetitionWorks);
            }
        }

        public bool IsInstitutesSelected
        {
            get { return isInstitutesSelected ; }
            set
            {
                isInstitutesSelected  = value;
                OnPropertyChanged();
                if (value )
                    PublishSectionChange(HubItemNames.InstitutesButton);
            }
        }

        public bool IsGrantPupilsSelected
        {
            get { return isGrantPupilsSelected; }
            set
            {
                if (value )
                    PublishSectionChange(HubItemNames.GrantPupilsButton);
                isGrantPupilsSelected = value;
                OnPropertyChanged();
            }
        }

        public bool IsAlumnusWith9ClassesSelected
        {
            get { return isAlumnusWith9ClassesSelected; }
            set
            {
                if (value )
                    PublishSectionChange(HubItemNames.AlumnusWith9ClassesButton);
                isAlumnusWith9ClassesSelected = value;
                OnPropertyChanged();
            }
        }

        private void PublishSectionChange(string viewName)
        {
            eventAggregator.GetEvent<HubPanelChangedEvent>().Publish(new HubPanelChangedEventArgs(viewName));
        }
        public ProceduresHubPanelViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;
        }

        protected ProceduresHubPanelViewModel()
        {}

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
        }
    }
}