﻿using System.Collections.ObjectModel;
using Prism.Events;
using Prism.Regions;

namespace SchoolSystem.Desktop.Modules.HubPanel.ViewModels
{
    public class AlumnusHubPanelViewModel
    {
        #region Constructors
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;
        public ObservableCollection<string> TypeOfEducation { get; set; } = new ObservableCollection<string>()
                                                                            {
                                                                                "Contract", "Budget",
                                                                                "Both"
                                                                            };
 
        #endregion


        public AlumnusHubPanelViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;

            // InitializeEvents();
        }

        protected AlumnusHubPanelViewModel()
        { }

        #endregion 
    }
}