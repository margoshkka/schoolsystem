﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;

namespace SchoolSystem.Desktop.Modules.HubPanel.ViewModels
{
    public class TopPupilsHubPanelViewModel:BaseViewModel
    {
        #region Constructors
        #region Fields

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        private readonly ISubjectService subjectService;
        private readonly IClassesService classesService;
        private readonly IMarkTypesService markTypesService;
        private readonly IMarkService markService;



        public ObservableCollection<Subject> Subjects { get; set; }
        public ObservableCollection<Class> Classes { get; set; }

        public ObservableCollection<Mark> Marks { get; set; }
        public ObservableCollection<MarkType> MarkTypes { get; set; }

        public ObservableCollection<string> ClassNames { get; set; }
        #endregion


        public TopPupilsHubPanelViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, ISubjectService subjectService, 
            IClassesService classesService, IMarkService markService, IMarkTypesService markTypesService)
        {
            this.eventAggregator = eventAggregator;
            this.regionManager = regionManager;

            // InitializeEvents();
            this.subjectService = subjectService;
            this.classesService = classesService;
            this.markService = markService;
            this.markTypesService = markTypesService;

            Subjects = new ObservableCollection<Subject>();
            Classes = new ObservableCollection<Class>();
            MarkTypes=new ObservableCollection<MarkType>();
            Marks=new ObservableCollection<Mark>();
            ClassNames=new ObservableCollection<string>();
        }

        protected TopPupilsHubPanelViewModel()
        { }

        #endregion
        public override async void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);

            Subjects.Clear();
            Subjects.Add(new Subject() {IdSubject = 1000000, Name="All subjects"});
            Subjects.AddRange((await subjectService.GetSubjectsAsync()).OrderBy(subject => subject.Name));

            Classes.Clear();
            Classes.Add(new Class(){ClassTypesId = 1,IdClass = 1000000, Letter = "All classes"});
            Classes.AddRange((await classesService.GetClassesAsync()).OrderBy(subject => subject.Number));
            GetClassnames();


            Marks.Clear();
            Marks.AddRange(await markService.GetMarksAsync());


            MarkTypes.Clear();
            MarkTypes.Add(new MarkType() {Name = "All types of mark"});
            MarkTypes.AddRange(await markTypesService.GetMarkTypesAsync());


        }

        void GetClassnames()
        {
            ClassNames.Clear();
            foreach (var @class in Classes)
            {
                if (@class.Number==0)
                {
                    ClassNames.Add(@class.Letter);
                }
                else
                    ClassNames.Add(@class.Number+" "+@class.Letter);
            }
        }
    }
}