﻿namespace SchoolSystem.Desktop.Modules.HubPanel.Constants
{
    public static class ViewNames
    {
        public const string PupilHubPanelView = "PupilHubPanelView";
        public const string TopPupilsHubPanelView = "TopPupilsHubPanelView";
        public const string ClassHubPanelView = "ClassHubPanelView";
        public const string AlumnusHubPanelView = "AlumnusHubPanelView";
        public const string AlumnusesHubPanelView = "AlumnusesHubPanelView";
        public const string ProceduresHubPanelView = "ProceduresHubPanelView";
        //public const string AuthorizationView = "AuthorizationView";
    }
}