﻿using System;
using Microsoft.Practices.Unity;
using Prism.Events;
using Prism.Regions;
using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;
using SchoolSystem.Desktop.Infrastructure.Events;
using SchoolSystem.Desktop.Infrastructure.Extensions;
using SchoolSystem.Desktop.Modules.Content.Constants;
using SchoolSystem.Desktop.Modules.HubPanel.ViewModels;
using SchoolSystem.Desktop.Modules.HubPanel.Views;
using NavigationParameters = Prism.Regions.NavigationParameters;
using ViewNames = SchoolSystem.Desktop.Modules.HubPanel.Constants.ViewNames;

namespace SchoolSystem.Desktop.Modules.HubPanel
{
    public class HubModule : Module
    {
        private readonly IEventAggregator eventAggregator;

        public HubModule(IUnityContainer container, IRegionManager regionManager, IEventAggregator eventAggregator)
            : base(container, regionManager)
        {
            this.eventAggregator = eventAggregator;
        }

        public override void Initialize()
        {
            base.Initialize();

            //RegionManager.RegisterViewWithRegion(RegionNames.HeaderRegion, typeof (MainHeaderView));
            RegisterViews();
            eventAggregator.GetEvent<HeaderChangedEvent>().Subscribe(OnHeaderChanged,
                ThreadOption.UIThread, true);

            eventAggregator.GetEvent<ContentChangedEvent>().Subscribe(OnContentChanged, ThreadOption.UIThread, true);
        }

        private void OnContentChanged(ContentChangedEventArgs obj)
        {
            switch (obj.SectionName)
            {
                case ContentItemNames.ClassItem:
                    RegionManager.RequestNavigate(RegionNames.HubRegion, new Uri(ViewNames.ClassHubPanelView, UriKind.Relative),
                                                  new NavigationParameters()
                                                  {
                                                      {
                                                          SchoolSystem.Desktop.Modules.Content.Constants.NavigationParameters.ClassParametr,
                                                          obj.DataContext
                                                      }
                                                  });
                    break;
                case ContentItemNames.PupilItem:
                    RegionManager.RequestNavigate(RegionNames.HubRegion, new Uri(ViewNames.PupilHubPanelView, UriKind.Relative),
                                                  new NavigationParameters()
                                                  {
                                                      {
                                                          SchoolSystem.Desktop.Modules.Content.Constants.NavigationParameters.PupilParametr,
                                                          obj.DataContext
                                                      }
                                                  });
                   

                    break;
            }
        }

        private void OnHeaderChanged(HeaderChangedEventArgs obj)
        {
            RegionManager.RequestNavigate(RegionNames.HubRegion, ViewNames.AlumnusesHubPanelView);
            switch (obj.SectionName)
            {
                case HeaderItemNames.PupilsButton:
                    RegionManager.RequestNavigate(RegionNames.HubRegion, ViewNames.TopPupilsHubPanelView);
                    break;
                case HeaderItemNames.ClassesButton:
                    //RegionManager.RequestNavigate(RegionNames.HubRegion, ViewNames.ClassHubPanelView);
                    RegionManager.Regions[RegionNames.HubRegion].Clear();
                    break;
                case HeaderItemNames.AlumnusButton:
                    RegionManager.RequestNavigate(RegionNames.HubRegion, ViewNames.AlumnusesHubPanelView);
                    break;
                case HeaderItemNames.AuthorizationButton:
                    RegionManager.Regions[RegionNames.HubRegion].Clear();
                    break;
                case HeaderItemNames.ProceduresButton:
                    RegionManager.RequestNavigate(RegionNames.HubRegion,ViewNames.ProceduresHubPanelView);
                    break;

            }
        }

        private void RegisterViews()
        {
            Container.RegisterType<object, PupilHubPanelView>(ViewNames.PupilHubPanelView);
            Container.RegisterType<PupilHubPanelViewModel>();

            Container.RegisterType<object, ClassHubPanelView>(ViewNames.ClassHubPanelView);
            Container.RegisterType<ClassHubPanelViewModel>();

            Container.RegisterType<object, TopPupilsHubPanelView>(ViewNames.TopPupilsHubPanelView);
            Container.RegisterType<TopPupilsHubPanelViewModel>();

            Container.RegisterType<object, AlumnusHubPanelView>(ViewNames.AlumnusHubPanelView);
            Container.RegisterType<AlumnusHubPanelViewModel>();

            Container.RegisterType<object, AlumnusesHubPanelView>(ViewNames.AlumnusesHubPanelView);
            Container.RegisterType<AlumnusesHubPanelViewModel>();

            Container.RegisterType<object, ProceduresHubPanelView>(ViewNames.ProceduresHubPanelView);
            Container.RegisterType<ProceduresHubPanelViewModel>();
        }

      
    }
}