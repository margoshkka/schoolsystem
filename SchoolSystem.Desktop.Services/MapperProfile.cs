﻿using AutoMapper;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;


namespace SchoolSystem.Desktop.Services
{
    public class MapperProfile : Profile
    {
    

        private static void Register<TSource, TValue>()
        {
            Mapper.CreateMap<TSource, TValue>();
            Mapper.CreateMap<TValue, TSource>();
        }



        protected override void Configure()
        {
            Register<PupilDto, Pupil>();
            Register<BasicPupilInfoDto, BasicPupilInfo>();
            Mapper.CreateMap<Pupil, BasicPupilInfoDto>();
            Mapper.CreateMap<Lesson, BasicLessonInfoDto>();
            Register<Lesson, LessonDto>();
            Register<Parent, ParentDto>();
            Register<BasicParentInfo, BasicParentInfoDto>();
            Mapper.CreateMap<Parent, BasicParentInfoDto>();
            Register<BasicLessonInfo, BasicLessonInfoDto>();
            Register<Subject, SubjectDto>();
            Register<ClassDto, Class>();
            Register<MarkType, MarkTypeDto>();
            Register<Mark, MarkDto>();
            Register<LessonStatusDto, LessonStatus>();
            Register<ClassType, ClassTypeDto>();
            Register<Alumnus, AlumnusDto>();
            Register<BasicAlumnusInfo, BasicAlumnusInfoDto>();
            Mapper.CreateMap<Alumnus, BasicAlumnusInfoDto>();

            ConfigureDeepCopy();
        }

        void ConfigureDeepCopy()
        {
            Mapper.CreateMap<Pupil, Pupil>();
            Mapper.CreateMap<Lesson, Lesson>();
            Mapper.CreateMap<Parent, Parent>();
            Mapper.CreateMap<Subject, Subject>();
            Mapper.CreateMap<Class, Class>();
            Mapper.CreateMap<Alumnus, Alumnus>();
            Mapper.CreateMap<ClassType,ClassType>();
            Mapper.CreateMap<LessonStatus,LessonStatus>();
            Mapper.CreateMap<MarkType,MarkType>();
            Mapper.CreateMap<MarkType,MarkType>();
    
        }
    }
}