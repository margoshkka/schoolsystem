﻿namespace SchoolSystem.Desktop.Services.Models
{
    public class SubjectDto
    {
        public int IdSubject { get; set; }
        public string Name { get; set; }
    }
}