﻿namespace SchoolSystem.Desktop.Services.Models
{
    public class LessonStatusDto
    {
        public int IdStatus { get; set; }
        public string StatusName { get; set; }
    }
}