﻿namespace SchoolSystem.Desktop.Services.Models
{
    public class ClassTypeDto
    {
        public int IdTypeOfClass { get; set; }
        public string Name { get; set; }
    }
}