﻿using System;

namespace SchoolSystem.Desktop.Services.Models
{
    public class PupilDto
    {
        public int IdPupil { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public Nullable<int> YearOfStartEducation { get; set; }
        public int ClassId { get; set; }
        public int FatherId { get; set; }
        public int MotherId { get; set; }
        public virtual ParentDto Parent { get; set; }
        public virtual ParentDto Parent1 { get; set; }
        public virtual ClassDto Class { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Lesson> Lessons { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Mark> Marks { get; set; }
        
    }
}