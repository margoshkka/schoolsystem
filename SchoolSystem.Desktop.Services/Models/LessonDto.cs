﻿using System;

namespace SchoolSystem.Desktop.Services.Models
{
    public class LessonDto
    {
        public int IdLesson { get; set; }
        public int PupilId { get; set; }
        public int SubjectId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public int LessonStatusId { get; set; }

        public virtual LessonStatusDto LessonStatus { get; set; }
        public virtual PupilDto Pupil { get; set; }
        public virtual SubjectDto Subject { get; set; }
    }
}