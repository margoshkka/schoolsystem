﻿namespace SchoolSystem.Desktop.Services.Models
{
    public class ClassDto
    {
        public int IdClass { get; set; }
        public string Letter { get; set; }
        public int Number { get; set; }
        public int ClassTypesId { get; set; }
        public int ClassYear { get; set; }
        public virtual ClassTypeDto ClassType { get; set; }
      
    }
}