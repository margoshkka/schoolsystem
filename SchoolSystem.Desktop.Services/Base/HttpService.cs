﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Core.Exceptions;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;
using ErrorDto = SchoolSystem.Desktop.Services.Models.ErrorDto;
using SecurityException = System.Security.SecurityException;

namespace SchoolSystem.Desktop.Services.Base
{
    public abstract class HttpService
    {
        protected IPermissionsService PermissionsService { get; private set; }
        protected string RootUrl { get; private set; }
        protected string RefreshTokenUrl { get; private set; }

        protected HttpService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService)
        {
            RootUrl = rootUrl;
            RefreshTokenUrl = refreshTokenUrl;
            PermissionsService = permissionsService;
        }

        /// <exception cref="Core.Exceptions.ApiException">Api exception inside http message.</exception>
        protected async Task EnsureSuccessStatusCodeAsync(HttpResponseMessage message)
        {
            if (!message.IsSuccessStatusCode)
            {
                string errorMessage = null;

                if (message.Content != null)
                {
                    var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                    if (!string.IsNullOrEmpty(content))
                    {
                        errorMessage = JsonConvert.DeserializeObject<ErrorDto>(content).Message;
                    }
                }
                throw new ApiException(errorMessage);
            }
        }

        #region GET

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> GetAsync(string url)
        {
            return GetAsync(url, true);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> GetAsync(string url, CancellationToken cancellationToken)
        {
            return GetAsync(url, true, cancellationToken);
        }

        /// <exception cref="ArgumentNullException"><paramref name="url" /> is null. </exception>
        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> GetAsync(string url, bool appendAccessToken)
        {
            return GetAsync(url, appendAccessToken, CancellationToken.None);
        }

        /// <exception cref="ArgumentNullException"><paramref name="url" /> is null. </exception>
        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task<HttpResponseMessage> GetAsync(string url, bool appendAccessToken, CancellationToken cancellationToken)
        {
            using (var httpClient = await CreateHttpClientAsync(appendAccessToken, cancellationToken).ConfigureAwait(false))
            {
                return await httpClient.GetAsync(new Uri(url), cancellationToken).ConfigureAwait(false);
            }
        }

        /// <exception cref="ArgumentNullException"><paramref name="link"/> is <see langword="null" />.</exception>
        /// <exception cref="ApiException">Api exception inside http message.</exception>
        public async Task<Stream> DownloadAsync([NotNull] string link)
        {
            if (link == null)
                throw new ArgumentNullException(nameof(link));

            using (var client = await CreateHttpClientAsync(true, CancellationToken.None).ConfigureAwait(false))
            {
                using (var message = await client.SendAsync(new HttpRequestMessage(HttpMethod.Get, link),
                                                            HttpCompletionOption.ResponseHeadersRead, CancellationToken.None).ConfigureAwait(false))
                {
                    await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                    var memoryStream = new MemoryStream();
                    await message.Content.CopyToAsync(memoryStream).ConfigureAwait(false);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    return memoryStream;
                }
            }
        }

        #endregion

        #region POST

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PostJsonAsync<TValue>(string url, TValue value)
        {
            return PostJsonAsync(url, value, true);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PostJsonAsync<TValue>(string url, TValue value, CancellationToken cancellationToken)
        {
            return PostJsonAsync(url, value, true, cancellationToken);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PostJsonAsync<TValue>(string url, TValue value, bool appendAccessToken)
        {
            return PostJsonAsync(url, value, appendAccessToken, CancellationToken.None);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task<HttpResponseMessage> PostJsonAsync<TValue>(string url, TValue value, bool appendAccessToken, CancellationToken cancellationToken)
        {
            var json = JsonConvert.SerializeObject(value);

            using (var httpClient = await CreateHttpClientAsync(appendAccessToken, cancellationToken).ConfigureAwait(false))
            {

                var content = new StringContent(json);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                return await httpClient.PostAsync(new Uri(url), content, cancellationToken).ConfigureAwait(false);
            }
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PostAsync(string url, HttpContent httpContent)
        {
            return PostAsync(url, httpContent, true);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PostAsync(string url, HttpContent httpContent, CancellationToken cancellationToken)
        {
            return PostAsync(url, httpContent, true, cancellationToken);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PostAsync(string url, HttpContent httpContent, bool appendAccessToken)
        {
            return PostAsync(url, httpContent, appendAccessToken, CancellationToken.None);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task<HttpResponseMessage> PostAsync(string url, HttpContent httpContent, bool appendAccessToken, CancellationToken cancellationToken)
        {
            using (var httpClient = await CreateHttpClientAsync(appendAccessToken, cancellationToken).ConfigureAwait(false))
            {
                return await httpClient.PostAsync(new Uri(url), httpContent, cancellationToken).ConfigureAwait(false);
            }
        }

        #endregion

        #region PUT

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PutJsonAsync<TValue>(string url, TValue value)
        {
            return PutJsonAsync(url, value, CancellationToken.None);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task<HttpResponseMessage> PutJsonAsync<TValue>(string url, TValue value, CancellationToken cancellationToken)
        {
            var json = JsonConvert.SerializeObject(value);

            using (var httpClient = await CreateHttpClientAsync(true, cancellationToken).ConfigureAwait(false))
            {
                var content = new StringContent(json);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                return await httpClient.PutAsync(new Uri(url), content, cancellationToken).ConfigureAwait(false);
            }
        }

        #endregion

        #region PATCH

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> PatchJsonAsync<TValue>(string url, TValue value)
        {
            return PatchJsonAsync(url, value, CancellationToken.None);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task<HttpResponseMessage> PatchJsonAsync<TValue>(string url, TValue value, CancellationToken cancellationToken)
        {
            var json = JsonConvert.SerializeObject(value);

            using (var httpClient = await CreateHttpClientAsync(true, cancellationToken).ConfigureAwait(false))
            {
                var method = new HttpMethod("PATCH");

                var content = new StringContent(json);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var request = new HttpRequestMessage(method, url)
                {
                    Content = content
                };

                return await httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
            }
        }

        #endregion

        #region DELETE

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task<HttpResponseMessage> DeleteAsync(string url)
        {
            return DeleteAsync(url, CancellationToken.None);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task<HttpResponseMessage> DeleteAsync(string url, CancellationToken cancellationToken)
        {
            using (var httpClient = await CreateHttpClientAsync(true, cancellationToken).ConfigureAwait(false))
            {
                return await httpClient.DeleteAsync(new Uri(url), cancellationToken).ConfigureAwait(false);
            }
        }

        #endregion

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        /// <exception cref="InvalidOperationException">Access token is not initialized.</exception>
        protected async Task<HttpClient> CreateHttpClientAsync(bool appendAccessToken, CancellationToken cancellationToken)
        {
            var client = new HttpClient();

            if (!appendAccessToken)
                return client;

            var accessToken = await PermissionsService.GetAccessTokenAsync().ConfigureAwait(false);
            if (accessToken == null)
                throw new InvalidOperationException("Access token is not initialized.");

            if (accessToken.IsValid)
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(accessToken.TokenType,
                    accessToken.Token);
            }
            else
            {
                await RefreshAccessTokenAsync(client, accessToken, cancellationToken).ConfigureAwait(false);
            }

            return client;
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected Task RefreshAccessTokenAsync(HttpClient client, AccessToken accessToken, CancellationToken cancellationToken)
        {
            return RefreshAccessTokenAsync(client, accessToken.RefreshToken, cancellationToken);
        }

        /// <exception cref="System.Security.SecurityException">UnsuccessStatusCode of refresh token response.</exception>
        protected async Task RefreshAccessTokenAsync(HttpClient client, string refreshToken,
            CancellationToken cancellationToken)
        {
            using (var result = await PostAsync(RefreshTokenUrl, new FormUrlEncodedContent(new[]
            {
            new KeyValuePair<string, string>("refresh_token", refreshToken),
            new KeyValuePair<string, string>("grant_type", "refresh_token"),
        }), false, cancellationToken).ConfigureAwait(false))
            {
                result.EnsureSuccessStatusCode();

                var content = await result.Content.ReadAsStringAsync().ConfigureAwait(false);

                var newToken = await UpdateAccessTokenAsync(content).ConfigureAwait(false);
                if (newToken.IsValid)
                {
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue(newToken.TokenType,
                            newToken.Token);

                    await PermissionsService.SetAccessTokenAsync(newToken).ConfigureAwait(false);
                }
                else
                {
                    throw new SecurityException(
                        $"New received access token is still not valid. Expiration time = '{newToken.ExpirationTime}'.");
                }
            }
        }

        protected async Task<AccessToken> UpdateAccessTokenAsync(string content)
        {
            dynamic json = JsonConvert.DeserializeObject(content);

            var token = new AccessToken((string)json.access_token,
                (string)json.refresh_token, (string)json.token_type,
                (string)json.userName, DateTime.Now.Add(TimeSpan.FromSeconds((double)json.expires_in - 120)));
            await PermissionsService.SetAccessTokenAsync(token).ConfigureAwait(false);
            return token;
        }
    }
    }
