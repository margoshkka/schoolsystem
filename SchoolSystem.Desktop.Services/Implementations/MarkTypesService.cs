﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = (typeof(IMarkTypesService)))]
    public class MarkTypesService:HttpService,IMarkTypesService
    {
        public MarkTypesService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task AddMarkTypeAsync([NotNull] MarkType markType)
        {
            if (markType == null)
                throw new ArgumentNullException(nameof(markType));
            var url = $"{RootUrl}/api/marktypes/";
            using (var message = await(PostJsonAsync(url, Mapper.Map<MarkTypeDto>(markType)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<MarkTypeDto>(content);
                markType.IdTypeOfMark = resultDto.IdTypeOfMark;
            }
          
        }

        public async Task<List<MarkType>> GetMarkTypesAsync()
        {
            var url = $"{RootUrl}/api/marktypes/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<MarkTypeDto[]>(content);
                return resultDto.Select(Mapper.Map<MarkType>).ToList();
            }
        }

        public async Task DeleteMarkAsync(int typeMarkId)
        {

            var url = $"{RootUrl}/api/marktypes/{typeMarkId}/";

            using (var message = await DeleteAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }
    }
}