﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = (typeof(IClassTypesService)))]
    public class ClassTypesService:HttpService,IClassTypesService
    {
        public ClassTypesService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task AddClassTypeAsync([NotNull] ClassType newClassType)
        {
            if (newClassType == null)
                throw new ArgumentNullException(nameof(newClassType));
            var url = $"{RootUrl}/api/classtypes/";
            using (var message = await (PostJsonAsync(url, Mapper.Map<ClassTypeDto>(newClassType)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<ClassTypeDto>(content);
                newClassType.IdTypeOfClass = resultDto.IdTypeOfClass;
            }
        }


        public async Task<List<ClassType>> GetClassTypesAsync()
        {
            var url = $"{RootUrl}/api/classtypes";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<ClassTypeDto[]>(content);
                return resultDto.Select(Mapper.Map<ClassType>).ToList();
            }
        }

        public async Task DeleteClassTypeAsync(int classTypeId)
        {
            var url = $"{RootUrl}/api/classtypes/{classTypeId}";
            using (var message = await DeleteAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }
    }
}