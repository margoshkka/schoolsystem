﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = typeof(ILessonsService))]
    public class LessonsServices:HttpService,ILessonsService
    {
        public LessonsServices(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task AddLessonAsync([NotNull] Lesson lesson)
        {
            if (lesson == null)
                throw new ArgumentNullException(nameof(lesson));
           
            var url = $"{RootUrl}/api/lessons/";
            using (var message = await(PostJsonAsync(url, Mapper.Map<LessonDto>(lesson)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<LessonDto>(content);
                lesson.IdLesson = resultDto.IdLesson;
            }
        }

        public async Task<List<Lesson>> GetLessonsAsync()
        {
            var url = $"{RootUrl}/api/lessons/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<LessonDto[]>(content);
                return resultDto.Select(Mapper.Map<Lesson>).ToList();
            }
        }

        public async Task<Lesson> GetLessonAsync(int lessonId)
        {
            var url = $"{RootUrl}/api/lessons/{lessonId}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<LessonDto>(content);

                var lesson    = Mapper.Map<Lesson>(result);
                return lesson;
            }
        }

        public async Task UpdateLessonAsync(int lessonId, BasicLessonInfo lessonInfo)
        {

            var url = $"{RootUrl}/api/lessons/{lessonId}/";

            using (var message = await PatchJsonAsync(url, Mapper.Map<BasicLessonInfo>(lessonInfo)).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }
    }
}