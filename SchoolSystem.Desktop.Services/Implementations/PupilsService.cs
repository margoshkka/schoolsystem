﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService (InterfaceType = (typeof(IPupilsService)))]
    public class PupilsService:HttpService,IPupilsService
    {
        public PupilsService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task<List<Pupil>> GetPupilsAsync()
        {
            var url = $"{RootUrl}/api/pupils/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<PupilDto[]>(content);
                return resultDto.Select(Mapper.Map<Pupil>).ToList();
            }
        }

        public async Task<Pupil> GetPupilAsync(int pupilId)
        {
            var url = $"{RootUrl}/api/pupils/{pupilId}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<PupilDto>(content);

                var pupil = Mapper.Map<Pupil>(result);
                return pupil;
            }
        }

        public async Task UpdatePupilAsync(int pupilId, BasicPupilInfo pupilInfo)
        {
            var url = $"{RootUrl}/api/pupils/{pupilId}/";

            using (var message = await PatchJsonAsync(url, Mapper.Map<BasicPupilInfoDto>(pupilInfo)).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }

        public async Task DeletePupilAsync(int pupilId)
        {

            var url = $"{RootUrl}/api/pupils/{pupilId}/";

            using (var message = await DeleteAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }

        public async Task<Pupil> AddPupilAsync([NotNull] Pupil pupil)
        {
            if (pupil == null)
                throw new ArgumentNullException(nameof(pupil));

              var url = $"{RootUrl}/api/pupils/";
            using (var message = await(PostJsonAsync(url, Mapper.Map<PupilDto>(pupil)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<PupilDto>(content);
                pupil.IdPupil = resultDto.IdPupil;
                return Mapper.Map<Pupil>(resultDto);
            }
        }

     
    }
}