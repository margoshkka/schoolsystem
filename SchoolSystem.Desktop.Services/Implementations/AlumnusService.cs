﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = (typeof(IAlumnusService)))]
    public class AlumnusService:HttpService,IAlumnusService
    {
        public AlumnusService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task AddAlumnusAsync([NotNull] Alumnus alumnus)
        {
            if (alumnus == null)
                throw new ArgumentNullException(nameof(alumnus));
            

            var url = $"{RootUrl}/api/alumnuses/";
            using (var message = await(PostJsonAsync(url, Mapper.Map<AlumnusDto>(alumnus)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<AlumnusDto>(content);
                alumnus.IdAlumnus = resultDto.IdAlumnus;
            }
        }

        public async Task<List<Alumnus>> GetAlumnusesAsync()
        {
            var url = $"{RootUrl}/api/alumnuses/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<AlumnusDto[]>(content);
                return resultDto.Select(Mapper.Map<Alumnus>).ToList();
            }
        }

        public async Task<Alumnus> GetAlumnusAsync(int idAlumnus)
        {
            var url = $"{RootUrl}/api/alumnuses/{idAlumnus}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<AlumnusDto>(content);

                var alumnus = Mapper.Map<Alumnus>(result);
                return alumnus;
            }
        }

        public async Task<List<Alumnus>> GetAlumnusesWithGrantAsync()
        {
            var url = $"{RootUrl}/api/alumnuses/withgrant";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<AlumnusDto[]>(content);
                return resultDto.Select(Mapper.Map<Alumnus>).ToList();
            }
        }

        public async Task<List<Alumnus>> GetAlumnusesWith9ClassesAsync()
        {
            var url = $"{RootUrl}/api/alumnuses/withnineclasses";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<AlumnusDto[]>(content);
                return resultDto.Select(Mapper.Map<Alumnus>).ToList();

            }}

        public async Task<double> GetProcentAlumnusesWithGrantAsync(int? year = null)
        {
            var url = $"{RootUrl}/api/alumnuses/procentwithgrant/{year}";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<double>(content);
                return resultDto;

            }
        }

        public async Task UpdateAlumnusAsync(int alumnusId, BasicAlumnusInfo alumnusInfo)
        {

            var url = $"{RootUrl}/api/alumnuses/{alumnusId}/";

            using (var message = await PatchJsonAsync(url, Mapper.Map<BasicAlumnusInfoDto>(alumnusInfo)).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }
    }
}