﻿using System.Threading;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    public class PermissionsService:IPermissionsService
    {
        private AccessToken accessToken;
        private readonly SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);


        public PermissionsService()
        {
        }

        public async Task<AccessToken> GetAccessTokenAsync()
        {
            await semaphoreSlim.WaitAsync().ConfigureAwait(false);

            try
            {
                return accessToken;
            }
            finally
            {
                semaphoreSlim.Release();
            }
        }

        public async Task SetAccessTokenAsync(AccessToken accessToken)
        {
            await semaphoreSlim.WaitAsync().ConfigureAwait(false);

            try
            {
                this.accessToken = accessToken;
            }
            finally
            {
                semaphoreSlim.Release();
            }
        }
    }
}