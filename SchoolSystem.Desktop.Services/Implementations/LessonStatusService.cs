﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = (typeof(ILessonStatusService)))]
    public class LessonStatusService:HttpService,ILessonStatusService
    {
       
        public LessonStatusService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task<LessonStatus> AddLessonStatusAsync([NotNull] LessonStatus lessonStatus)
        {
            if (lessonStatus == null)
                throw new ArgumentNullException(nameof(lessonStatus));

            var url = $"{RootUrl}/api/lessonstatuses/";
            using (var message = await(PostJsonAsync(url, Mapper.Map<LessonStatusDto>(lessonStatus)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<LessonStatusDto>(content);
                lessonStatus.IdStatus = resultDto.IdStatus;
                return Mapper.Map<LessonStatus>(resultDto);
            }

        }

        public async Task DeleteLessonStatusAsync(int idLessonStatus)
        {
            var url = $"{RootUrl}/api/lessonstatuses/{idLessonStatus}/";

            using (var message = await DeleteAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }

        public async Task<List<LessonStatus>> GetLessonStatusesAsync()
        {
            var url = $"{RootUrl}/api/lessonstatuses/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<LessonStatusDto[]>(content);
                return resultDto.Select(Mapper.Map<LessonStatus>).ToList();
            }
        }
    }
}