﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService (InterfaceType = typeof(IMarkService))]
    public class MarkService:HttpService,IMarkService
    {
        public MarkService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task<Mark> AddMarkAsync([NotNull] Mark mark)
        {
            if (mark == null)
                throw new ArgumentNullException(nameof(mark));
            var url = $"{RootUrl}/api/marks/";
            using (var message = await(PostJsonAsync(url, Mapper.Map<MarkDto>(mark)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<MarkDto>(content);
                mark.IdMark = resultDto.IdMark;
                return Mapper.Map<Mark>(resultDto);
            }
        }

        public async Task<List<Mark>> GetMarksAsync()
        {
            var url = $"{RootUrl}/api/marks/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<MarkDto[]>(content);
                return resultDto.Select(Mapper.Map<Mark>).ToList();
            }
        }

        public async Task<Mark> GetMarkAsync(int markId)
        {
            var url = $"{RootUrl}/api/marks/{markId}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<MarkDto>(content);

                var mark = Mapper.Map<Mark>(result);
                return mark;
            }
        }

        public async Task<List<Mark>> FilterMarksAsync(int? pupilId, int? subjectId, int? classId)
        {
            var url = $"{RootUrl}/api/marks/filter?pupilId={pupilId}&subjectId={subjectId}&classId={classId}";

            using (var message = await(GetAsync(url).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<List<MarkDto>>(content);
                return Mapper.Map<List<Mark>>(resultDto);
            }
        }

        public async Task<List<Mark>> GetTopPupilsAsync()
        {
            var url = $"{RootUrl}/api/marks/toppupils";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<MarkDto[]>(content);
                return resultDto.Select(Mapper.Map<Mark>).ToList();
            }
        }
    }
}