﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType =typeof(IParentsService))]
    public class ParentsService:HttpService,IParentsService
    {
        public ParentsService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task<Parent> GetParentAsync(int parentId)
        {
            var url = $"{RootUrl}/api/parents/{parentId}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<ParentDto>(content);

                var parent = Mapper.Map<Parent>(result);
                return parent;
            }
        }

        public async Task UpdateParentAsync(int parentId, BasicParentInfo parentInfo)
        {
            var url = $"{RootUrl}/api/parents/{parentId}/";

            using (var message = await PatchJsonAsync(url, Mapper.Map<BasicParentInfo>(parentInfo)).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }

        public async Task AddParentAsync(Parent parent)
        {
            if (parent == null)
                throw new ArgumentNullException(nameof(parent));
            var url = $"{RootUrl}/api/parents/";
            using (var message = await (PostJsonAsync(url, Mapper.Map<ParentDto>(parent)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<ParentDto>(content);
                parent.IdParent = resultDto.IdParent;
            }
        }
    }
}