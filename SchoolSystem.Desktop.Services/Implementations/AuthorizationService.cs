﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Exceptions;
using SchoolSystem.Desktop.Services.Core.Interfaces;

namespace SchoolSystem.Desktop.Services.Implementations
{

    [HttpService(InterfaceType = typeof(IAuthorizationService))]
    public class AuthorizationService:HttpService,IAuthorizationService
    {
        public AuthorizationService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        { }

        /// <exception cref="ApiException">Api exception inside http message.</exception>
        public async Task LogInAsync(string login, string password)
        {
            var url = $"{RootUrl}/token";

            using (var message = await PostAsync(url, new FormUrlEncodedContent(new[]
                                                                                {
                                                                                    new KeyValuePair<string, string>("username", login),
                                                                                    new KeyValuePair<string, string>("password", password),
                                                                                    new KeyValuePair<string, string>("grant_type", "password"),
                                                                                }), false).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                await UpdateAccessTokenAsync(await message.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        public async Task RegisterAsync(string email, string password, string passwordConfirm, string role)
        {
            var url = $"{RootUrl}/api/Account/Register?role={role}";
            using (var message=await PostJsonAsync(url,new {Password=password, ConfirmPassword=passwordConfirm,Email=email}))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }

        public async Task<string> GetRoleAsync(string email)
        {
            var url = $"{RootUrl}/api/Account/Roles?email={email}";
            using (var message = await GetAsync(url, false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content=await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                return content.Replace("/", String.Empty).Replace("\\", String.Empty).Replace(" ",String.Empty).Replace("\"",String.Empty);
            }
        }
    }
}