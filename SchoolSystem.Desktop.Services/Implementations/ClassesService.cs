﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Exceptions;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = typeof(IClassesService))]
    public class ClassesService:HttpService,IClassesService
    {
        public ClassesService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}



        public async Task<Class>  AddClassAsync([NotNull] Class newClass)
        {
            if (newClass == null)
                throw new ArgumentNullException(nameof(newClass));
            var url = $"{RootUrl}/api/classes/";
            using (var message= await (PostJsonAsync(url,Mapper.Map<ClassDto>(newClass)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<ClassDto>(content);
                newClass.IdClass = resultDto.IdClass    ;
                return Mapper.Map<Class>(resultDto);
            }
        }

        public async Task<List<Class>> GetClassesAsync()
        {

            var url = $"{RootUrl}/api/classes/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<ClassDto[]>(content);
                return resultDto.Select(Mapper.Map<Class>).ToList();
            }
        }

        public async Task<Class> GetClassAsync(int idClass)
        {
            var url = $"{RootUrl}/api/classes/{idClass}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<ClassDto>(content);

                var @class = Mapper.Map<Class>(result);
                return @class;
            }
        }

        public async Task DeleteClassAsync(int idClass)
        {
            var url = $"{RootUrl}/api/classes/{idClass}";
            using (var message = await DeleteAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
            }
        }
    }
}