﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using Newtonsoft.Json;
using SchoolSystem.Desktop.Services.Attributes;
using SchoolSystem.Desktop.Services.Base;
using SchoolSystem.Desktop.Services.Core.Interfaces;
using SchoolSystem.Desktop.Services.Core.Models;
using SchoolSystem.Desktop.Services.Models;

namespace SchoolSystem.Desktop.Services.Implementations
{
    [HttpService(InterfaceType = (typeof(ISubjectService)))]
    public class SubjectsService:HttpService,ISubjectService
    {
        public SubjectsService(string rootUrl, string refreshTokenUrl, IPermissionsService permissionsService) : base(rootUrl, refreshTokenUrl, permissionsService)
        {}

        public async Task AddSubject([NotNull] Subject newSubject)
        {
            if (newSubject == null)
                throw new ArgumentNullException(nameof(newSubject));

            var url = $"{RootUrl}/api/subjects/";
            using (var message = await (PostJsonAsync(url, Mapper.Map<SubjectDto>(newSubject)).ConfigureAwait(false)))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<SubjectDto>(content);
                newSubject.IdSubject = resultDto.IdSubject;
            }
        }

        public async Task<List<Subject>> GetSubjectsAsync()
        {
            var url = $"{RootUrl}/api/subjects/";
            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);
                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var resultDto = JsonConvert.DeserializeObject<SubjectDto[]>(content);
                return resultDto.Select(Mapper.Map<Subject>).ToList();
            }
        }

        public async Task<Subject> GetSubjectAsync(int idSubject)
        {
            var url = $"{RootUrl}/api/subjects/{idSubject}/";

            using (var message = await GetAsync(url).ConfigureAwait(false))
            {
                await EnsureSuccessStatusCodeAsync(message).ConfigureAwait(false);

                var content = await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                var result = JsonConvert.DeserializeObject<SubjectDto>(content);

                var subject = Mapper.Map<Subject>(result);
                return subject;
            }
        }
    }
}