﻿using AutoMapper;
using SchoolSystem.WebAPI.Core;

namespace SchoolSystem.Web
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.AddProfile<MapperConfiguration>();
        }
    }
}