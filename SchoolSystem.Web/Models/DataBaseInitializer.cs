﻿using System.Data.Entity;
using System.Linq;
using SchoolSystem.WebAPI.Core.Constants;

namespace SchoolSystem.Web.Models
{
    public class DataBaseInitializer:IDatabaseInitializer<ApplicationDbContext>
    {
        public void InitializeDatabase(ApplicationDbContext context)
        {
            AddRoleIfNotExists(context, Roles.Administrator);
            AddRoleIfNotExists(context, Roles.Guest);
            AddRoleIfNotExists(context, Roles.MainTeacher);
            AddRoleIfNotExists(context, Roles.Teacher);

            context.SaveChanges();
        }

        private void AddRoleIfNotExists(ApplicationDbContext context, string roleName)
        {
            if (context.Roles.Any(role => role.Name == roleName))
                return;
            var identityRole = context.Roles.Create();
            identityRole.Name = roleName;
            context.Roles.Add(identityRole);
        }
    }
}