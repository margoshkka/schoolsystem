﻿using Prism.Events;

namespace SchoolSystem.Desktop.Infrastructure.Events
{
    public class ContentChangedEvent : PubSubEvent<ContentChangedEventArgs>
    { }

    public class ContentChangedEventArgs
    {
        #region Properties, Indexers

        public string SectionName { get; set; }
        public object DataContext { get; set; }

        #endregion

        #region Constructors

        public ContentChangedEventArgs(string sectionName, object dataContext=null)
        {
            SectionName = sectionName;
            DataContext = dataContext;
        }

        #endregion
    }
}