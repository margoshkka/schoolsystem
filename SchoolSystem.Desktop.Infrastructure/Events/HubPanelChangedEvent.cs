﻿using System;
using Prism.Events;

namespace SchoolSystem.Desktop.Infrastructure.Events
{
    public class HubPanelChangedEvent : PubSubEvent<HubPanelChangedEventArgs>
    {}

    public class HubPanelChangedEventArgs
    {
        #region Properties, Indexers

        public string SectionName { get; set; }
        public object DataContext { get; set; }

        #endregion

        #region Constructors

        public HubPanelChangedEventArgs(string sectionName, object dataContext=null)
        {
            SectionName = sectionName;
            DataContext = dataContext;
        }

        #endregion
    }
}