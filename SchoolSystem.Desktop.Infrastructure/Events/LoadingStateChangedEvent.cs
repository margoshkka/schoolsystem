﻿using Prism.Events;

namespace SchoolSystem.Desktop.Infrastructure.Events
{
    public class LoadingStateChangedEvent : PubSubEvent<LoadingStateChangedEventArgs>
    {}

    public class LoadingStateChangedEventArgs
    {
        #region Properties, Indexers

        public bool IsLoading { get; private set; }

        public string Text { get; private set; }

        #endregion

        #region Constructors

        public LoadingStateChangedEventArgs(bool isLoading)
        {
            IsLoading = isLoading;
        }

        public LoadingStateChangedEventArgs(bool isLoading, string text)
        {
            IsLoading = isLoading;
            Text = text;
        }

        #endregion
    }
}