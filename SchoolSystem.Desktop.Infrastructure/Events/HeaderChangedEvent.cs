﻿using System;
using Prism.Events;

namespace SchoolSystem.Desktop.Infrastructure.Events
{
    public class HeaderChangedEvent : PubSubEvent<HeaderChangedEventArgs>
    {}

    public class HeaderChangedEventArgs
    {
        #region Properties, Indexers

        public string SectionName { get; set; }

        #endregion

        #region Constructors

        public HeaderChangedEventArgs(string sectionName)
        {
            SectionName = sectionName;
        }

        #endregion
    }
}