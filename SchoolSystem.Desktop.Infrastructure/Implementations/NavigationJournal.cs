﻿using System;
using System.Collections.Generic;
using Prism.Regions;

namespace SchoolSystem.Desktop.Infrastructure.Implementations
{
    public class NavigationJournal : IRegionNavigationJournal
    {
        internal const string BackInternalKey = "prism:back";
        internal const string ForwardInternalKey = "prism:forward";

        private readonly Stack<IRegionNavigationJournalEntry> backStack = new Stack<IRegionNavigationJournalEntry>();
        private readonly Stack<IRegionNavigationJournalEntry> forwardStack = new Stack<IRegionNavigationJournalEntry>();

        private bool isNavigatingInternal;

        /// <summary>
        /// Gets or sets the target that implements INavigate.
        /// </summary>
        /// <value>The INavigate implementation.</value>
        /// <remarks>
        /// This is set by the owner of this journal.
        /// </remarks>
        public INavigateAsync NavigationTarget
        {
            get; set;
        }

        /// <summary>
        /// Gets the current navigation entry of the content that is currently displayed.
        /// </summary>
        /// <value>The current entry.</value>
        public IRegionNavigationJournalEntry CurrentEntry
        {
            get; private set;
        }

        /// <summary>
        /// Gets a value that indicates whether there is at least one entry in the back navigation history.
        /// </summary>
        /// <value><c>true</c> if the journal can go back; otherwise, <c>false</c>.</value>
        public bool CanGoBack => this.backStack.Count > 0;

        /// <summary>
        /// Gets a value that indicates whether there is at least one entry in the forward navigation history.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance can go forward; otherwise, <c>false</c>.
        /// </value>
        public bool CanGoForward => this.forwardStack.Count > 0;

        /// <summary>
        /// Navigates to the most recent entry in the back navigation history, or does nothing if no entry exists in back navigation.
        /// </summary>
        public void GoBack()
        {
            if (!this.CanGoBack)
                return;
            var entry = this.backStack.Peek();
            this.InternalNavigate(entry, result =>
            {
                if (!result)
                    return;
                if (this.CurrentEntry != null)
                {
                    this.forwardStack.Push(this.CurrentEntry);
                }
                this.backStack.Pop();
                this.CurrentEntry = entry;
            }, BackInternalKey);
        }

        /// <summary>
        /// Navigates to the most recent entry in the forward navigation history, or does nothing if no entry exists in forward navigation.
        /// </summary>
        public void GoForward()
        {
            if (!this.CanGoForward)
                return;
            var entry = this.forwardStack.Peek();
            this.InternalNavigate(entry, result =>
            {
                if (!result)
                    return;
                if (this.CurrentEntry != null)
                {
                    this.backStack.Push(this.CurrentEntry);
                }

                this.forwardStack.Pop();
                this.CurrentEntry = entry;
            }, ForwardInternalKey);
        }

        /// <summary>
        /// Records the navigation to the entry..
        /// </summary>
        /// <param name="entry">The entry to record.</param>
        public void RecordNavigation(IRegionNavigationJournalEntry entry)
        {
            if (this.isNavigatingInternal)
                return;
            if (this.CurrentEntry != null)
            {
                this.backStack.Push(this.CurrentEntry);
            }

            this.forwardStack.Clear();
            this.CurrentEntry = entry;
        }

        /// <summary>
        /// Clears the journal of current, back, and forward navigation histories.
        /// </summary>
        public void Clear()
        {
            this.CurrentEntry = null;
            this.backStack.Clear();
            this.forwardStack.Clear();
        }

        private void InternalNavigate(IRegionNavigationJournalEntry entry, Action<bool> callback, string internalKey)
        {
            this.isNavigatingInternal = true;
            this.NavigationTarget.RequestNavigate(entry.Uri, nr =>
            {
                this.isNavigatingInternal = false;

                if (nr.Result.HasValue)
                {
                    callback(nr.Result.Value);
                }
            }, CreateNavigationParameters(entry.Parameters, internalKey));
        }

        private NavigationParameters CreateNavigationParameters(NavigationParameters parameters, string internalKey)
        {
            var backNavigationParameters = new NavigationParameters();
            foreach (var navigationParameter in parameters)
            {
                backNavigationParameters.Add(navigationParameter.Key, navigationParameter.Value);
            }

            backNavigationParameters.Add(internalKey, string.Empty);

            return backNavigationParameters;
        }
    }
}