﻿using SchoolSystem.Desktop.Infrastructure.Base;
using SchoolSystem.Desktop.Infrastructure.Constants;

namespace SchoolSystem.Desktop.Infrastructure.UserInfo
{
    public class CurrentUser:BaseViewModel
    {
        private string role;
        private bool isLoggedIn;

        public string Role
        {
            set
            {
                if (value == role)
                    return;
                role = value;
                OnPropertyChanged();
            }
            get { return role; }
        }

        public bool IsLoggedIn
        {
            get { return isLoggedIn; }
            set
            {
                if (value == isLoggedIn)
                    return;
                isLoggedIn = value;
                OnPropertyChanged();
            }
        }
    }
}