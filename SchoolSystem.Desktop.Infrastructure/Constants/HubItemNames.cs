﻿using System.Data;

namespace SchoolSystem.Desktop.Infrastructure.Constants
{
    public static class HubItemNames
    {
        public const string PupilSearchButton = "PupilSearchButton";
        public const string PupilContactInfoButton = "PupilContactInfoButton";
        public const string PupilMarksButton = "PupilMarksButton";
        public const string PupilMissedLessonsButton = "PupilMissedLessonsButton";

        public const string AlumnusSearchButton = "AlumnusSearchButton";
        public const string AlumnusContactInfoButton = "AlumnusContactInfoButton";
        public const string AlumnusEducationButton = "AlumnusEducationButton";
        public const string AlumnusGraduationYearTextBox = "AlumnusGraduationYearTextBox";
        public const string AlumnusCompletedClassesTextbox = "AlumnusCompletedClassesTextbox";
        public const string AlumnusTypeOfEducationCombobox = "AlumnusTypeOfEducationCombobox";

        public const string ClassSearchButton = "ClassSearchButton";
        public const string ClassPupilsButton = "ClassPupilsButton";
        public const string ClassJournalButton = "ClassJournalButton";

        public const string TopPupilsSubjectsComboBox = "TopPupilsSubjectsComboBox";
        public const string TopPupilsClassesComboBox = "TopPupilsClassesComboBox";
        public const string TopPupilsWorksComboBox = "TopPupilsWorksComboBox";
        public const string TopPupilsMinimumScoreTextBox = "TopPupilsMinimumScoreTextBox";

        public const string PupilPersonalDealButton = "PupilPersonalDealButton";
        public const string TimesheetButton = "TimesheetButton";
        public const string MarksJournalButton = "MarksJournalButton";
        public const string MarkReportButton = "MarkReportButton";
        public const string RateCompetitionWorks = "RateCompetitionWorksButton";
        public const string InstitutesButton = "InstitutesButton";
        public const string GrantPupilsButton = "GrantPupilsButton";
        public const string AlumnusWith9ClassesButton = "AlumnusWith9ClassesButton";
    }
}