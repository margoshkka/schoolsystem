﻿namespace SchoolSystem.Desktop.Infrastructure.Constants
{
    public static class Roles
    {
        public const string Guest = "Guest";
        public const string Teacher = "Teacher";
        public const string MainTeacher = "MainTeacher";
        public const string Administrator = "Administrator";
    }
}