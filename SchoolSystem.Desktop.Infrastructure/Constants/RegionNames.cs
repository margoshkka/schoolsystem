﻿namespace SchoolSystem.Desktop.Infrastructure.Constants
{
    public static class RegionNames
    {
        public const string HeaderRegion="HeaderRegion";
        public const string ContentRegion = "ContentRegion";
        public const string HubRegion = "HubRegion";
    }
}