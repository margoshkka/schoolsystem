﻿namespace SchoolSystem.Desktop.Infrastructure.Constants
{
    public static class HeaderItemNames
    {
        public const string PupilsButton = "PupilsButton";
        public const string ClassesButton = "ClassesButton";
        //public const string TopPupilsButton = "TopPupilsButton";
        public const string AlumnusButton = "AlumnusButton";
        public const string AuthorizationButton = "AuthorizationButton";
        public const string RegistrationButton = "RegistrationButton";
        public const string MarkTypesButton = "MarkTypesButton";
        public const string MarksButton = "MarksButton";
        public const string ClassTypesButton = "ClassTypesButton";
        public const string LessonStatusesButton = "LessonStatusesButton";
        public const string ProceduresButton = "ProceduresButton";
    }
}