﻿using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Prism.Mvvm;
using Prism.Regions;

namespace SchoolSystem.Desktop.Infrastructure.Base
{
    public abstract class BaseViewModel:BindableBase,INavigationAware
    {
        public virtual void OnNavigatedTo(NavigationContext navigationContext)
        {
        }

        public virtual bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public virtual void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }

        [NotifyPropertyChangedInvocator]
        protected override void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
        }
    }
}