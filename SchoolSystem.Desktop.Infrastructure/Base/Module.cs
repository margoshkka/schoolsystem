﻿using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;

namespace SchoolSystem.Desktop.Infrastructure.Base
{
    public class Module : IModule
    {
        protected IUnityContainer Container
        {
            get; private set;
        }
        protected IRegionManager RegionManager
        {
            get; private set;
        }

        protected Module(IUnityContainer container, IRegionManager regionManager)
        {
            Container = container;
            RegionManager = regionManager;
        }

        public virtual void Initialize()
        {
        }
    }
}