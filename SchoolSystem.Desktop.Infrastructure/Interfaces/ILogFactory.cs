﻿using System;

namespace SchoolSystem.Desktop.Infrastructure.Interfaces
{
    public interface ILogFactory
    {
        ILogger GetLogger(Type context);
        ILogger GetLogger<TContext>();
    }
}