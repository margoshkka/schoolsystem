﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Prism.Regions;

namespace SchoolSystem.Desktop.Infrastructure.Extensions
{
    public static class RegionExtensions
    {
        /// <summary>
        /// Remove all views from region.
        /// </summary>
        /// <param name="region">Region instance.</param>
        public static void Clear(this IRegion region)
        {
            foreach (var viewObject in region.Views.ToArray())
            {
                var member = viewObject as IRegionMemberLifetime;
                if (member != null && !member.KeepAlive)
                    continue;

                var view = viewObject as FrameworkElement;
                var viewModelMember = view?.DataContext as IRegionMemberLifetime;
                if(viewModelMember != null && !viewModelMember.KeepAlive)
                    continue;

                region.Remove(view);
            }
        }

        /// <summary>
        /// Deactivate all views in region.
        /// </summary>
        /// <param name="region">Region instance.</param>
        public static void Deactivate(this IRegion region)
        {
            foreach (var view in region.Views)
            {
                region.Deactivate(view);
            }
        }

        /// <summary>
        /// Activate all views in region.
        /// </summary>
        /// <param name="region">Region instance.</param>
        public static void Activate(this IRegion region)
        {
            foreach (var view in region.Views)
            {
                region.Activate(view);
            }
        }

        public static bool IsOnView(this IRegion region, string viewName)
        {
            return region.ActiveViews.Any(
                item => item.GetType().Name.Equals($"{viewName}View", StringComparison.InvariantCulture));
        }
    }
}
